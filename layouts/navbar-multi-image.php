<?php 
$lhi = get_field('left_header_image', 'options');
$lhiDark = get_field('left_header_image_dark', 'options');
$field = get_field_object('display_options_for_left_header_image', 'options');
$value = $field['value'];
$value_output = implode(" ", $value);

//Check if there is a mobile-specific image loaded
$mobile_image = get_field('mobile_header_image', 'options');
$mobile_image_dark = get_field('mobile_header_image_dark', 'options');

?>

<?php if (is_mobile()) {
	if (is_front_page() || is_home()) :
		if(!empty($mobile_image_dark)): ?>
			<img src="<?php echo $mobile_image_dark['url'];?>" class="mobileHeaderImageStyle"/>
		<?php else: ?>
			<img src="<?php echo $mobile_image['url'];?>" class="mobileHeaderImageStyle" id="mobileHeaderImage"/>
		<?php endif; // end check for mobile_image_dark
	else: ?>
		<img src="<?php echo $mobile_image['url'];?>" class="mobileHeaderImageStyle" id="mobileHeaderImage"/>
	<?php endif; // end is_front_page or is_home
} else { ?>
	<div class="branding-image" id="navbar-multi-image">
	<?php if (is_front_page() || is_home()): ?>
		<img src="<?php echo $lhiDark['url'];?>" class="<?php echo $value_output; ?>" id="leftDarkHeaderImage">
	<?php else: ?>
		<img src="<?php echo $lhi['url'];?>" class="<?php echo $value_output; ?>" id="leftHeaderImage">
	<?php endif; ?>
	<?php if (is_front_page() || is_home()):
		if(!empty($mobile_image_dark)): ?>
			<img src="<?php echo $mobile_image_dark['url'];?>" class="mobileHeaderImageStyle" id="mobileHeaderImage"/>
		<?php else: ?>
			<img src="<?php echo $mobile_image['url'];?>" class="mobileHeaderImageStyle homeInvert" id="mobileHeaderImage"/>
		<?php endif; // end check for mobile_image_dark ?>
		<img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php bloginfo( 'name' ); ?>" id="mainImage">
	<?php else: ?>
		<img src="<?php echo $mobile_image['url'];?>" class="mobileHeaderImageStyle" id="mobileHeaderImage" />
		<img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="<?php bloginfo( 'name' ); ?>" id="mainImage">
	<?php endif; ?>
	</div>
<?php } ?>