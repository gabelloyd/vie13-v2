<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Red_Hook_Crit
 */

get_header(); ?>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Sorry, that page can&rsquo;t be found.', 'vie13' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p class="text-center"><?php esc_html_e( 'Try searching for your phrase again or make your search more specific?', 'vie13' ); ?></p>

					<?php
						echo '<div class="text-center">';
						get_search_form();
						echo '</div>';
						the_widget( 'WP_Widget_Recent_Posts' );

						?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
