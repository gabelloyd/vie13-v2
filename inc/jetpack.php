<?php
/**
 * Jetpack Compatibility File.
 *
 * @link https://jetpack.me/
 *
 * @package Vie13_Theme_2
 */

/**
 * Jetpack setup function.
 *
 * See: https://jetpack.me/support/infinite-scroll/
 * See: https://jetpack.me/support/responsive-videos/
 */
function vie13_jetpack_setup() {
	// Add theme support for Infinite Scroll.
	add_theme_support( 'infinite-scroll', array(
		//'type' => 'click',
		'container' => 'main',
		'render'    => 'vie13_infinite_scroll_render',
		'footer'    => false,
		'wrapper'   => true,
		'posts_per_page' => 10, // change this to override the 7 posts loaded by infinite scroll 
	) );

	// Add theme support for Responsive Videos.
	add_theme_support( 'jetpack-responsive-videos' );
}
add_action( 'after_setup_theme', 'vie13_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function vie13_infinite_scroll_render() {
	// while ( have_posts() ) {
	// 	the_post();
	// 	if ( is_search() ) :
	// 	    get_template_part( 'template-parts/post/content', 'search' );
	// 	else :
	// 	    get_template_part( 'template-parts/post/content', get_post_format() );
	// 	endif;
	// }

	get_template_part( 'template-parts/post/content', get_post_format() );
}
