<?php
/**
 * Sample implementation of the Custom Header feature.
 *
 * You can add an optional custom header image to header.php like so ...
 *
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package vie13
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses vie13_header_style()
 */
function vie13_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'vie13_custom_header_args', array(
		'default-image' 		=> get_template_directory_uri() . '/assets/images/default_header.jpg',
		'uploads'       		=> true,
		'width'                 => 1480,
		'height'                => 200,
		'flex-height'           => true,
		'video'					=> true,
		'wp-head-callback'       => 'vie13_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'vie13_custom_header_setup' );

if ( ! function_exists( 'vie13_header_style' ) ) :
/**
 * Styles the header image and text displayed on the blog.
 *
 * @see vie13_custom_header_setup().
 */
function vie13_header_style() {
	$header_text_color = get_header_textcolor();

	// If no custom options for text are set, let's bail.
	// get_header_textcolor() options: add_theme_support( 'custom-header' ) is default, hide text (returns 'blank') or any hex value.
	if ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color ) {
		return;
	}

	// If we get this far, we have custom styles. Let's do this.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
	?>
		.site-title,
		.site-description {
			position: absolute;
			clip: rect(1px, 1px, 1px, 1px);
		}
	<?php
		// If the user has set a custom color for the text use that.
		else :
	?>
		.site-title a,
		.site-description {
			color: #<?php echo esc_attr( $header_text_color ); ?>;
		}
	<?php endif; ?>
	</style>
	<?php
}
endif;
