<?php
/**
 * Vie13 Theme Customizer.
 *
 * @package Vie13_Theme_2
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function vie13_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';


	/**
	 * COLORS PANEL
	 */
		// Create nav background color setting
		$wp_customize->add_setting('nav_bg_color', array(
			'default' 			=> '#FFFFFF',
			'type' 				=> 'theme_mod',
			'sanitize_callback' => 'sanitize_hex_color',
			'transport' 		=> 'postMessage',
			));

		// Add Control for Navbar Color
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'nav_bg_color', array(
					'label' => __('Main Nav Background Color', 'vie13'),
					'description' => 'Set the base color for the main nav.',
					'section' => 'colors',
					)
				)
			);
		
		// Create nav link color setting
		$wp_customize->add_setting('nav_link_color', array(
			'default' 			=> '#FFFFFF',
			'type' 				=> 'theme_mod',
			'sanitize_callback' => 'sanitize_hex_color',
			'transport' 		=> 'postMessage',
			));

		// Add Control for Navbar Link Color
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'nav_link_color', array(
					'label' => __('Main Nav Link Colors', 'vie13'),
					'description' => 'Set the base color for the main nav links.',
					'section' => 'colors',
					)
				)
			);
		// Create Navbar link:hover, link:focus color setting
		$wp_customize->add_setting('nav_link_hf_color', array(
			'default' 			=> '#555555',
			'type' 				=> 'theme_mod',
			'sanitize_callback' => 'sanitize_hex_color',
			'transport' 		=> 'postMessage',
			));

		// Add Control for Navbar Link:hover Text Color
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'nav_link_hf_color', array(
					'label' => __('Main Nav Link (Hover) Color', 'vie13'),
					'description' => 'When you hover over a link, it should change color.',
					'section' => 'colors',
					)
				)
			);
		// Create Navbar link:hover, link:focus color setting
		$wp_customize->add_setting('primary-color', array(
			'default' 			=> '#bada55',
			'type' 				=> 'theme_mod',
			'sanitize_callback' => 'sanitize_hex_color',
			'transport' 		=> 'postMessage',
			));

		// Add Control for Navbar Link:hover Text Color
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'primary-color', array(
					'label' => __('Primary Color', 'vie13'),
					'description' => 'Main color for the theme.',
					'section' => 'colors',
					)
				)
			);
		// Create Navbar link:hover, link:focus color setting
		$wp_customize->add_setting('secondary-color', array(
			'default' 			=> '#fff200',
			'type' 				=> 'theme_mod',
			'sanitize_callback' => 'sanitize_hex_color',
			'transport' 		=> 'postMessage',
			));

		// Add Control for Navbar Link:hover Text Color
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
				$wp_customize,
				'secondary-color', array(
					'label' => __('Secondary Color', 'vie13'),
					'description' => 'Secondary color for the theme.',
					'section' => 'colors',
					)
				)
			);
	/**
	* CUSTOM FOOTER SECTION
	*/

	// Register New Section
	$wp_customize->add_section( 'spf_footer_options', 
         array(
            'title' => __( 'Footer Options', 'vie13' ), //Visible title of section
            'priority' => 36, //Determines what order this appears in
            'capability' => 'edit_theme_options', //Capability needed to tweak
            'description' => __('Allows you to customize the footer.', 'vie13'), //Descriptive tooltip
         ) 
      );

	// Create footer background color setting
	$wp_customize->add_setting('footer_bg_color', array(
		'default' 			=> '#FFFFFF',
		'type' 				=> 'theme_mod',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' 		=> 'postMessage',
		));

	// Add Control for Footer Color
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_bg_color', array(
				'label' => __('Footer Background Color', 'vie13'),
				'section' => 'spf_footer_options',
				)
			)
		);

	// Create footer text color setting
	$wp_customize->add_setting('footer_text_color', array(
		'default' 			=> '#000000',
		'type' 				=> 'theme_mod',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' 		=> 'postMessage',
		));

	// Add Control for Footer Text Color
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_text_color', array(
				'label' => __('Footer Text Color', 'vie13'),
				'section' => 'spf_footer_options',
				)
			)
		);

	// Create footer link color setting
	$wp_customize->add_setting('footer_link_color', array(
		'default' 			=> '#333333',
		'type' 				=> 'theme_mod',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' 		=> 'postMessage',
		));

	// Add Control for Footer Text Color
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_link_color', array(
				'label' => __('Footer Link Color', 'vie13'),
				'section' => 'spf_footer_options',
				)
			)
		);
	// Create footer link:hover, link:focus color setting
	$wp_customize->add_setting('footer_link_hf_color', array(
		'default' 			=> '#555555',
		'type' 				=> 'theme_mod',
		'sanitize_callback' => 'sanitize_hex_color',
		'transport' 		=> 'postMessage',
		));

	// Add Control for Footer link:hover, link:focus Text Color
	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'footer_link_hf_color', array(
				'label' => __('Footer Link (Hover) Color', 'vie13'),
				'section' => 'spf_footer_options',
				)
			)
		);
}
add_action( 'customize_register', 'vie13_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function vie13_customize_preview_js() {
	wp_enqueue_script( 'vie13_customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'vie13_customize_preview_js' );


/** 
 * Inject Customizer CSS
 */

function vie13_kustom_css() {
	$primary_color			= get_theme_mod('primary-color' );
	$secondary_color		= get_theme_mod('secondary-color');
	$nav_bg_color 			= get_theme_mod('nav_bg_color');
	$nav_bg_image			= get_theme_mod('nav_bg_image');
		$nav_bg_image_repeat	= get_theme_mod('nav_bg_image_repeat');
		$nav_bg_image_pos		= get_theme_mod('nav_bg_image_pos');
		$nav_bg_image_size		= get_theme_mod('nav_bg_image_size');		
	$nav_link_color 		= get_theme_mod('nav_link_color');
	$nav_link_hf_color		= get_theme_mod('nav_link_hf_color');
	$footer_bg_color 		= get_theme_mod('footer_bg_color');
	$footer_text_color 		= get_theme_mod('footer_text_color');
	$footer_link_color 		= get_theme_mod('footer_link_color');
	$footer_link_hf_color 	= get_theme_mod('footer_link_hf_color');
	?>
	<style type="text/css">
		:root {
			--primary-color: <?php echo $primary_color; ?>;
			--secondary-color: <?php echo $secondary_color; ?>;
		}
		#site-navigation,
		ul.sidebar_login_links,
		.navbar-nav .open .dropdown-menu {
			background-color: <?php echo $nav_bg_color; ?>;
		}
		#site-navigation a,
		ul.sidebar_login_links a,
		li.widget_wp_sidebarlogin {
			color: <?php echo $nav_link_color; ?>;
		}
		#site-navigation a:hover, 
		#site-navigation a:focus, 
		#site-navigation a:active, 
		#site-navigation li.active a,
		ul.sidebar_login_links a:hover,
		ul.sidebar_login_links a:focus,
		ul.sidebar_login_links a:active,
		#site-navigation li.current-menu-item a {
			color: <?php echo $nav_link_hf_color; ?>;
		}
		.site-footer {
			background-color: <?php echo $footer_bg_color; ?>;
			color: <?php echo $footer_text_color; ?>;
		}
		.site-footer a {
			color: <?php echo $footer_link_color; ?>;
		}
		.site-footer a:hover, .site-footer a:focus, .site-footer a:active {
			color: <?php echo $footer_link_hf_color; ?>;
		}
	</style>
<?php

	
}
add_action('wp_head', 'vie13_kustom_css');
