<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Vie13_Theme_2
 */

if ( ! function_exists( 'vie13_time_link' ) ) :
/**
 * Gets a nicely formatted string for the published date.
 */
function vie13_time_link() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		get_the_date( DATE_W3C ),
		get_the_date(),
		get_the_modified_date( DATE_W3C ),
		get_the_modified_date()
	);

	// Wrap the time string in a link, and preface it with 'Posted on' for screen readers .
	return sprintf(
		/* translators: %s: post date */
		__( '<span class="screen-reader-text">Posted on</span> %s', 'vie13' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);
}
endif;

if ( ! function_exists( 'vie13_time_display' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function vie13_time_display() {
	
	// Finally, let's write all of this to the page.
	echo '<div layout="row center-justify">';
	echo '<span class="posted-on">' . vie13_time_link() . '</span>';
	echo '</div>';
}
endif;

if ( ! function_exists( 'vie13_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function vie13_posted_on() {

	$byline = sprintf(
		esc_html_x( '%s', 'post author', 'vie13' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	// Finally, let's write all of this to the page.
	echo '<div layout="row center-justify">';
	echo '<span class="byline"> ' . $byline . '</span><span class="posted-on">' . vie13_time_link() . '</span>';
	echo '</div>';
}
endif;

if ( ! function_exists( 'vie13_social_meta' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function vie13_social_meta() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		echo '<div layout="row center-justify">'; // start this container here 
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '#', esc_html__( ' #', 'vie13' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( '%1$s', 'vie13' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	
		// Comments logic for these posts 
		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link( esc_html__( '', 'vie13' ), esc_html__( '1 Comment', 'vie13' ), esc_html__( '% Comments', 'vie13' ) );
			echo '</span>';
			echo '</div>'; // end the container for layout here 
		} else {
			echo '</div>'; // end the container here if comments are not loaded 
		}
	} // this ends the post check. We won't show any of this on pages. Use vie13_entry_meta to show meta on pages.

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'vie13' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);

}
endif;

if ( ! function_exists( 'vie13_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function vie13_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'vie13' ) );
		if ( $categories_list && vie13_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'vie13' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '#', esc_html__( ' #', 'vie13' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( '%1$s', 'vie13' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'vie13' ), esc_html__( '1 Comment', 'vie13' ), esc_html__( '% Comments', 'vie13' ) );
		echo '</span>';
	}
	if ( 'post' === get_post_type() ) {
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'vie13' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="post-edit-link">',
			'</span>'
		);
	} else {
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'vie13' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function vie13_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'vie13_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'vie13_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so vie13_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so vie13_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in vie13_categorized_blog.
 */
function vie13_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'vie13_categories' );
}
add_action( 'edit_category', 'vie13_category_transient_flusher' );
add_action( 'save_post',     'vie13_category_transient_flusher' );
