# Welcome to Vie13 custom Wordpress theme #
Base theme for Vie13 Kustom Apparel. Developed by @gabelloyd

We are building this on Underscores. SASS is the preprocessing language. Bootstrap v3.3.7 is the framework.

We try to keep this as DRY as possible. 

# Summary #

Custom Wordpress theme with WooCommerce support. Integration with Instagram for product galleries are part of this project. 

Custom post types are supported. 

## Contributing ##

Fork stuff onto your own branch. Work on it. Submit a pull-request.

## Issues ##

We'll work faster if we all see what we all see. Use Bitbucket's Issue Tracker if you see something, even if you are the one who forks and fixes it.

Don't be afraid to ask questions. If something looks stupid or you can't figure out why something does what it does, ask. 

# Changelog #

### 1.3.4 - February 23, 2018 ###
* update archive-product to v3.3.0
* Remove older JS files that could conflict with lazy loading
* Update woocommerce template files to 3.3.0
* Clean up a lot of the JS files. Try to remove files not being used.
* Styling updates for fixed. Remove woo-single-product because not using. Tweak styling for store notice.

### 1.3.3 - January 31, 2018 ###
* Drop in a snippet that seemed to be missing from WC on `archive-product.php`. Addresses Issue #121
* Clarify comments and notations in `archive-product.php`
* Resolve issue #122 - tablepress styling corruption
* Update Readme for 1.3.3 updates
* Correct comment in `functions.php` about where a JS script is located

### 1.3.2 - November 16, 2017 ###
* Resolve Issue #120 - stop query of all posts on product tags if there is not a tag set for product 

### 1.3 - November 13, 2017 ###
* Overhaul IG posting styling and approach. Leveraging more of the stored tags and featured images than putting everything in `the_content`. 

### 1.2.2 - November 13, 2017 ###
* Change `post-status` call in `woocommerce/single-product/product-thumbnails` to `publish`

### 1.2.1 - November 8, 2017 ###
* Slight tweak to columns to remove overflow-y styling

### 1.2 - November 7, 2017 (Election Day!) ###
* Update for Issue #116 to make headers for categories taller
* Update custom logo with. Resolve Issue #117
* Remove .entry-content img from styling to resolve Issue #73
* Remove hidden images on .content img. Not a complete fix but resolves Issues #103 #92
* Align checkbox in form. Resolve Issue #118

### 1.1.1 - July 14, 2017 (Bastille Day!) ###
* Correct issue with product page styling not being specific enough. Resolve Issue #115

### 1.1 - July 14, 2017 (Bastille Day!) ###
* Remove carousel arrow controls from display
* Change typekit and stylesheet calls to try and get WPE to render better
* setup `set_url_scheme` wrapper around source for wc default image
* add trac ticket number used to help resolve Issue #102
* Resolve Issue #98 - widen woo-category-tab-wrapper
* Remove related products. Resolve Issue #104
* Add default custom header image
* Setup custom_header image as background to header.
* Finalize custom header stylig and control. Resolve Issue #95
* Correct width on single column mixin
* Comment out the product-thumbnails override for now to allow thumbnails to display
* Remove sorting
* Correct typo in query for `woo_get_product_instagram_photos`
* add function to hide admin bar for users who are not admin
* Change header alt tag to site name
* Starting on IG id integartions
* using `get_the_terms_list` to check the product tags on the page in product-thumbnails.php
* Resolve Issue #31 - instagram images on the product gallery (still needs styling)
* Resolve Issue #105 for now
* update styling to resolve issue #106
* Resolve issue #70
* Resolve Issue #107
* Change order of merging array on product page
* Correct issue that the #carousel-feature id was not passed to the carousel
* add another widget area for eu-cookies
* update for 3.1 `product-thumbnail`
* Resolve Issue #110 - svg upload
** -- also styled new graphic for header
* adding styling for EU Cookie banner
* add some style to the eu-cookie-banner. Resolve Issue #108
* Resolve Issue #111 - add primary and secondary variable colors to Customizer
* Resolve Issue #112 - hide IG thumbs on mobile, optimize the page.

### 1.0 - April 25, 2017 ###
* Initial release