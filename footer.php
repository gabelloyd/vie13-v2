<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Vie13_Theme_2
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			
			<?php get_template_part('template-parts/footer/content', 'navbar-footer');?>
			<div layout="row bottom-center">
				<div>
					<?php if(get_field('copyright_by_text', 'option')): ?>
						<span class="site-name">&#169; <?php echo date('Y')?>&nbsp;<?php echo the_field('copyright_by_text', 'option')?></span>
					<?php else: ?>
						<span class="site-name">&#169; <?php echo date('Y')?>&nbsp;<?php echo get_bloginfo('name')?>, LLC </span>
				<?php endif; ?>
				</div>
			</div> <!-- /layout -->
		</div><!--.container-->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
