=== Vie13 ===

Contributors: gabelloyd
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.4
Tested up to: 4.9.2
Stable tag: 1.3.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called Red Hook Crit based on underscores by Automattic.

== Description ==

Custom Wordpress theme for Red Hook Crit.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Red Hook Crit includes support for Infinite Scroll in Jetpack.

== Changelog ==

### 1.3.3 - January 31, 2017 ###
* Resolve issue #122
* Resolve issue #121

### 1.3.2 - November 16, 2017 ###
* Resolve Issue #120 - stop query of all posts on product tags if there is not a tag set for product 

### 1.3 - November 13, 2017 ###
* Overhaul IG posting styling and approach. Leveraging more of the stored tags and featured images than putting everything in `the_content`. 

### 1.2.2 - November 13, 2017 ###
* Change `post-status` call in `woocommerce/single-product/product-thumbnails` to `publish`

### 1.2.1 - November 8, 2017 ###
* Slight tweak to columns to remove overflow-y styling

### 1.2 - November 7, 2017 (Election Day!) ###
* Update for Issue #116 to make headers for categories taller
* Update custom logo with. Resolve Issue #117
* Remove .entry-content img from styling to resolve Issue #73
* Remove hidden images on .content img. Not a complete fix but resolves Issues #103 #92
* Align checkbox in form. Resolve Issue #118

### 1.1.1 - July 14, 2017 (Bastille Day!) ###
* Correct issue with product page styling not being specific enough. Resolve Issue #115

### 1.1 - July 14, 2017 (Bastille Day!) ###
* Remove carousel arrow controls from display
* Change typekit and stylesheet calls to try and get WPE to render better
* setup `set_url_scheme` wrapper around source for wc default image
* add trac ticket number used to help resolve Issue #102
* Resolve Issue #98 - widen woo-category-tab-wrapper
* Remove related products. Resolve Issue #104
* Add default custom header image
* Setup custom_header image as background to header.
* Finalize custom header stylig and control. Resolve Issue #95
* Correct width on single column mixin
* Comment out the product-thumbnails override for now to allow thumbnails to display
* Remove sorting
* Correct typo in query for `woo_get_product_instagram_photos`
* add function to hide admin bar for users who are not admin
* Change header alt tag to site name
* Starting on IG id integartions
* using `get_the_terms_list` to check the product tags on the page in product-thumbnails.php
* Resolve Issue #31 - instagram images on the product gallery (still needs styling)
* Resolve Issue #105 for now
* update styling to resolve issue #106
* Resolve issue #70
* Resolve Issue #107
* Change order of merging array on product page
* Correct issue that the #carousel-feature id was not passed to the carousel
* add another widget area for eu-cookies
* update for 3.1 `product-thumbnail`
* Resolve Issue #110 - svg upload
** -- also styled new graphic for header
* adding styling for EU Cookie banner
* add some style to the eu-cookie-banner. Resolve Issue #108
* Resolve Issue #111 - add primary and secondary variable colors to Customizer
* Resolve Issue #112 - hide IG thumbs on mobile, optimize the page.

### 1.0 - April 25, 2017 ###
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
