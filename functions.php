<?php

/**
 * Red Hook Crit functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Vie13_Theme_2
 */

if ( ! function_exists( 'vie13_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function vie13_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Red Hook Crit, use a find and replace
	 * to change 'vie13' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'vie13', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size('max-fullsize', 1600, 1024); //1400x934
	add_image_size('fixed-height-gallery', 1400, 500, true); //WxH = 1400x500 (14:5 ratio) hard crop
	add_image_size('medium-square', 300, 300, true); // 300x300 cropped. Used for Instagram
	///////////////////////////////////////////// Theme Thumbnails are 
	// Thumbnail 150x150 crop 
	// medium : 300x300 no crop 
	// large : 1024 x 1024 no crop 
	///////////////////////////////////////////// WooCommerce Thumbs are 
	// shop_catalog		Catalog Images: 150x150 Hard crop 
	// shop_single 		Single Product Image 300x300 hard crop 
	// shop_thumbnail 	Product Thumbnails: 90x90 hard crop 

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' 		=> esc_html__('Primary', 'vie13' ),
		'social' 		=> esc_html__('Social', 'vie13'),
		'menu-widget-1'	=> esc_html__('Menu Widget 1', 'vie13' ),
		'menu-widget-2'	=> esc_html__('Menu Widget 2', 'vie13' ),
		'menu-widget-3'	=> esc_html__('Menu Widget 3', 'vie13' ),
		'menu-widget-4' => esc_html__('Menu Widget 4', 'vie13' ),
		'wc-menu-cart'	=> esc_html__('WC Menu Cart', 'vie13' ),
	) );

	//Load Custom Walker menu to get Boostrap classes in place
	require get_template_directory() . '/inc/walker_menu.php';

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		// 'quote',
		// 'link',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 300,
		'height'      => 100,
		'flex-width'  => true,
		'flex-height'  => true,
	) );

	// Add theme support for Custom header.
	add_theme_support( 'custom-header' );

	// Custom Editor Styling
	add_editor_style();

	/**
	 * Add WooCommerce Support
	 * http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
	 */

	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
endif;
add_action( 'after_setup_theme', 'vie13_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function vie13_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'vie13_content_width', 700 );
}
add_action( 'after_setup_theme', 'vie13_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function vie13_widgets_init() {
	register_sidebar( array(
        'name' 			=> esc_html__( 'Navbar Widget 1st Row', 'vie13' ),
        'id' 			=> 'navbar-widgets-1',
        'description' 	=> esc_html__( 'Displays at the very top of the page', 'vie13' ),
        'before_widget' => '<div class="navbar-super-widget">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	    ) );
	register_sidebar( array(
        'name' 			=> esc_html__( 'Navbar Widget 2nd Row', 'vie13' ),
        'id' 			=> 'navbar-widgets',
        'description' 	=> esc_html__( 'Display same row as the custom logo', 'vie13' ),
        'before_widget' => '<li id="navbar-widget-item" class="widget" self="center">',
		'after_widget'  => '</li>',
		'before_title'  => '',
		'after_title'   => '',
	    ) );
	register_sidebar( array(
        'name' 			=> esc_html__( 'Navbar Widget 3rd Row', 'vie13' ),
        'id' 			=> 'navbar-widgets-3',
        'description' 	=> esc_html__( 'Display same row as the main menu', 'vie13' ),
        'before_widget' => '<div id="navbar-widget-item" class="widget search-container" self="right">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	    ) );
	register_sidebar( array(
        'name' 			=> esc_html__( 'Sticky Navbar Widget', 'vie13' ),
        'id' 			=> 'navbar-widgets-sticky',
        'description' 	=> esc_html__( 'Sticky navbar right side', 'vie13' ),
        'before_widget' => '<div id="widget-item" class="widget sticky-navbar">',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	    ) );
    register_sidebar( array(
        'name' => esc_html__( 'WooCommerce Sidebar', 'vie13' ),
        'id' => 'sidebar-2',
        'description' => esc_html__( 'Widgets in this area will be shown on WooCommerce Shopping pages (not catalog).', 'vie13' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
		'after_widget'  => '</li>',
		'before_title'  => '<h2 class="woo-widgettitle">',
		'after_title'   => '</h2>',
    ) );
       /**
    	* Creates a sidebar
    	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
    	*/
    	$footer_widget_1 = array(
    		'name'          => __( 'Footer Option 1', 'vie13' ),
    		'id'            => 'footer-option-1',
    		'description'   => '',
    		'class'         => '',
    		'before_widget' => '<div id="%1s" class="widget %2s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2 class="widgettitle">',
    		'after_title'   => '</h2>'
    	);
    
    	register_sidebar( $footer_widget_1 );

    	/**
    	* Creates a sidebar
    	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
    	*/
    	$footer_widget_2 = array(
    		'name'          => __( 'Footer Option 2', 'vie13' ),
    		'id'            => 'footer-option-2',
    		'description'   => '',
    		'class'         => '',
    		'before_widget' => '<div id="%1s" class="widget %2s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2 class="widgettitle">',
    		'after_title'   => '</h2>'
    	);
    
    	register_sidebar( $footer_widget_2 );

    	/**
    	* Creates a sidebar
    	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
    	*/
    	$footer_widget_3 = array(
    		'name'          => __( 'Footer Option 3', 'vie13' ),
    		'id'            => 'footer-option-3',
    		'description'   => '',
    		'class'         => '',
    		'before_widget' => '<div id="%1s" class="widget %2s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2 class="widgettitle">',
    		'after_title'   => '</h2>'
    	);
    
    	register_sidebar( $footer_widget_3 );

    	/**
    	* Creates a sidebar
    	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
    	*/
    	$footer_widget_4 = array(
    		'name'          => __( 'Footer Option 4', 'vie13' ),
    		'id'            => 'footer-option-4',
    		'description'   => '',
    		'class'         => '',
    		'before_widget' => '<div id="%1s" class="widget %2s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2 class="widgettitle">',
    		'after_title'   => '</h2>'
    	);
    
    	register_sidebar( $footer_widget_4 );

    	/**
    	* Creates a sidebar
    	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
    	*/
    	$eu_cookie_widget = array(
    		'name'          => __( 'EU Cookie Widget Area', 'vie13' ),
    		'id'            => 'eu-cookie-widget-area',
    		'description'   => '',
    		'class'         => '',
    		'before_widget' => '<div id="%1s" class="widget %2s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2 class="widgettitle">',
    		'after_title'   => '</h2>'
    	);
    
    	register_sidebar( $eu_cookie_widget );
    
}
add_action( 'widgets_init', 'vie13_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function vie13_scripts() {

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'vie13-navigation', get_template_directory_uri() . '/assets/js/min/navigation.min.js', array(), '20151215', true );

	if ( !is_admin() ) : // this if statement will insure the following code only gets added to your wp site and not the admin page cause your code has no business in the admin page right unless that's your intentions
    // jquery
	wp_deregister_script('jquery'); // this deregisters the current jquery included in wordpress
	wp_register_script('jquery','https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', array(), '2.2.4', true); // this registers the replacement jquery and we put it in the header
	wp_enqueue_script('jquery'); // you can either let wp insert this for you or just delete this and add it directly to your template

	//Masonry comes packaged with WP. Enqueue to get it going.
	wp_enqueue_script('masonry'); // part of WP Core. Just have to get it.
	wp_enqueue_script('imagesloaded', get_stylesheet_directory_uri() . '/bower_components/imagesloaded/imagesloaded.pkgd.min.js', array('masonry', 'jquery'), '4.1.1', true);

	//Scripts for Styling
	wp_enqueue_script('compiled', get_template_directory_uri().'/assets/js/min/_compiled.min.js', array('jquery'), wp_get_theme()->get('Version'), true); // all our custom scripts compiled
	
	// Bootstrap v3.3.7
	wp_enqueue_script('bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
	wp_enqueue_style('bootstrap-style','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', null, '3.3.7', 'all');

	// waypoints
	wp_enqueue_script('waypoints', get_template_directory_uri().'/bower_components/waypoints/lib/jquery.waypoints.min.js', array('jquery'), '4.0.1', true);
	// wp_enqueue_script('sticky_waypoints',get_template_directory_uri().'/bower_components/waypoints/lib/shortcuts/sticky.min.js',array('jquery', 'waypoints'), '4.0.1', true);
	// wp_enqueue_script('inview_waypoints',get_template_directory_uri().'/bower_components/waypoints/lib/shortcuts/inview.min.js', array('jquery', 'waypoints'), '4.0.1', true);
	// wp_enqueue_script('infinite_waypoints',get_template_directory_uri().'/bower_components/waypoints/lib/shortcuts/infinite.min.js', array('jquery', 'waypoints'), '4.0.1', true);

	

	//Helper Assets
	wp_enqueue_style('fla',get_template_directory_uri().'/assets/libs/flex-layout-attribute-master/css/flex-layout-attribute.min.css', array(), '1.0.3', 'screen'); // FLA library

	// WooComerce Script helpers 
	// 1. Homepage slide the category tabs up onto the carousel
	if (is_home() || is_front_page()) {
		wp_enqueue_script( 'woo-nav-tab', get_template_directory_uri().'/assets/js/min/woo-nav-tabs.min.js', array( 'jquery' ), wp_get_theme()->get('Version'), true);
	}

	// 2. Infinite Scroll for WC
	// Requires YITH Infinite Scroll Plugin 
	// This adds classes to the images to make sure they appear and get Jetpack infinite scroll
	// Seems to conflict with Jetpack infinite scroll, though :\ 
	if (is_product_category()) {
		wp_enqueue_script( 'woo-yith-is-custom', get_stylesheet_directory_uri() . '/assets/js/min/woo-yith-is.min.js', array( 'jquery' ), wp_get_theme()->get('Version'), true );
	}
	// END WooCommerce Helper Functions 

	// Flickity
	wp_enqueue_script( 'flickity', get_template_directory_uri().'/bower_components/flickity/dist/flickity.pkgd.min.js', array( 'jquery' ), '2.0.5', true);
	wp_enqueue_style('flickity-style',get_template_directory_uri().'/bower_components/flickity/dist/flickity.min.css', null, '2.0.5', 'screen');

	//print stylesheet, which we compile to from assets/sass/style.scss
	// wp_enqueue_style( 'print', get_template_directory_uri().'/assets/css/print.css.min', array(), wp_get_theme()->get('Version'), 'print' );

	//required stylesheet for WP, which we compile to from assets/sass/style.scss
	wp_enqueue_style( 'vie13-style', get_stylesheet_uri(), array(), wp_get_theme()->get('Version'), 'all' );

	endif; // end just the admin wrapper

}
add_action( 'wp_enqueue_scripts', 'vie13_scripts' );

/**
 * Conditional IE Stylesheets
 * Ref: http://wpscholar.com/blog/ie-conditional-stylesheets-wordpress/
 */

//add_action( 'wp_enqueue_scripts', 'enqueue_ie_styles' );
//function enqueue_ie_styles() {
    
//    global $wp_styles;
    
    /**
     * Load our IE specific stylesheet for a range of newer versions:
     * <!--[if gt IE 8]> ... <![endif]-->
     * <!--[if gte IE 9]> ... <![endif]-->
     * NOTE: You can use the 'greater than' or the 'greater than or equal to' syntax here interchangeably.
     */
//    wp_enqueue_style( 'rhc-ie', get_stylesheet_directory_uri() . "/assets/css/ie.css");
//    $wp_styles->add_data( 'rhc-ie', 'conditional', 'IE' );
    
// }

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
// require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Infinite Scroll file.
 */

require get_template_directory() . '/assets/functions/function-infinite-scroll.php';

/**
 * Load TinyMCE Customizations file.
 */
require get_template_directory() . '/assets/functions/functions-tinymce.php';

/**
 * Load Custom Editor Styles file.
 */
require get_template_directory() . '/assets/functions/functions-add-editor-styles.php';

/**
 * Load ACF Customization file.
 */
require get_template_directory() . '/assets/functions/functions-acf-customizations.php';

/**
 * Load WP Customization file.
 */
require get_template_directory() . '/assets/functions/functions-wp-customizations.php';

/**
 * Typekit the proper way
 */
include_once get_template_directory() . '/assets/functions/function-typekit.php';

/**
 * Fontawesome the proper way
 */
include_once get_template_directory() . '/assets/functions/function-fontawesome.php';

/**
 * Instagram Feed
 */
include_once get_template_directory() . '/assets/functions/function-instagram.php';

/**
 * WooCommerce Category Tabs
 */
include_once get_template_directory() . '/assets/functions/woocommerce/wc-category-nav-panel.php';

/**
 * WooCommerce General Changes
 */
include_once get_template_directory() . '/assets/functions/woocommerce/woo-customizations.php';

/**
 * WooCommerce Instagram Images
 */
include_once get_template_directory() . '/assets/functions/woocommerce/woo-instagram.php';

/**
 * White Label Admin
 */
include_once get_template_directory() . '/assets/functions/white-label-admin.php';

/**
 * Get title_to_id function
 */

include_once get_template_directory() . '/assets/functions/title_to_id.php';

//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// SIDECART LOGIN ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

// Let's add a class to the body if we are not logged in
// Ref: http://www.joelwolfgang.com/adding-a-logged-out-class-to-wordpress-body-tag/
add_filter('body_class','ltc_logged_out');
function ltc_logged_out($classes) {
    if (! ( is_user_logged_in() ) ) {
        $classes[] = 'logged-out';
    }
    return $classes;
}

// Wordpress already adds a class called "logged-in" to the body, so we don't have to do anything else.


// Filter Reference for Sidecart-login

// Wrap the title in a span when logged in
add_filter('sidebar_login_widget_logged_in_title', 'span_sidebar_login_widget_logged_in_title');

function span_sidebar_login_widget_logged_in_title($title){
	$title = '<a id="loggedInWrapper" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sidebar_logged_in_title">'.$title.'</span></a>';
		// We insertAfter the avatar using jQuery in jquery.styles.js
	return $title;
}

// Wrap the title in a span when logged OUT
add_filter('sidebar_login_widget_logged_out_title', 'span_sidebar_login_widget_logged_out_title');

function span_sidebar_login_widget_logged_out_title($title){
	$title = '<a class="sidebar_logged_out_title" id="loggedOutLabel" type="button" data-toggle="modal" data-target="#loginModal">'.$title.'</a>';

	return $title;
}

// Prepend Bootstrap Modal HTML to the login form
add_filter('sidebar_login_widget_wp_login_form', 'prepend_modal_sidebar_login_widget_wp_login_form');

function prepend_modal_sidebar_login_widget_wp_login_form($before_form) {
	
	$before_form = '<!-- Modal -->
		<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loggedOutLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="loginLabel">Login to '.get_bloginfo('name').'</h4>
					</div> <!-- modal-header -->
				<div class="modal-body">
      '.$before_form;
	return $before_form;
}

// Append Bootstrap Modal HTML to the login form
add_filter('sidebar_login_widget_wp_login_form', 'append_modal_sidebar_login_widget_wp_login_form');

function append_modal_sidebar_login_widget_wp_login_form($after_form){
	
	$after_form .= '</div><!-- modal-body -->
					</div><!-- modal-content -->
					</div><!-- modal-dialog -->
					</div><!-- modal-fade -->';
	return $after_form;
}

/* Remove Admin Bar for admins 
 * 
 * @since 1.0
 */

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

// ADD SUPPORT FOR SVG UPLOAD
// https://css-tricks.com/snippets/wordpress/allow-svg-through-wordpress-media-uploader/

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');