<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package vie13
 * @since 1.0
 * @version 1.0
 * @author Gabe Lloyd
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main" >

		<?php
		while ( have_posts() ) : the_post();
			
			get_template_part( 'template-parts/post/content', get_post_format() );

			// If comments are open or we have at least one comment, load up the comment template.
			$format = get_post_format(); // store the post format here because we keep using this through out the page
			if ( comments_open() || get_comments_number() ) :
				if($format == 'image' || 'video'){ // If the format is image then don't show it
					// do nothing
				} else {
					comment_form(); // show the form because we are on every other format type.
				}
			endif;
			the_post_navigation( array(
				'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'vie13' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'vie13' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper"><span class="nav-post-indicator glyphicon glyphicon-chevron-left"></span>%title</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'vie13' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'vie13' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">%title<span class="nav-post-indicator glyphicon glyphicon-chevron-right"></span></span>',
			) );
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
