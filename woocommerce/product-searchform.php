<?php
/**
 * The template for displaying product search form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/product-searchform.php.
 *
 * Using Bootstrap to style this form for Vie13
 * Also changing the "Search" to the glyphicon magnifying glass on mobile 
 * http://getbootstrap.com/components/#input-groups-buttons
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="input-group">
	<form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
		<label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
		<input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field form-control" placeholder="<?php echo esc_attr_x( 'Search Products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
		
		<span class="input-group-btn">
			<?php if (!is_mobile()): ?>
			<input class="btn btn-default" type="submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>" />
			<?php elseif(is_mobile()): ?>
			<button class="btn btn-default btn-search-button" type="submit">
				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
				<span class="screen-reader-text"><?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?></span>
			</button>
			<?php endif; ?>
		</span>
		<input type="hidden" name="post_type" value="product" />
	</form>
</div>