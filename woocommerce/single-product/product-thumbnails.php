<?php
/**
 * Single Product Thumbnails with Instagram Posts 
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.2
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $post, $product;

// 1. Get all Product Tags 
// 2. Use product tags terms in a WP_Terms_Query to get posts with the same terms 
// 3. Get those post IDs 
// 4. Put those Post IDs into an array_merge with the gallery IDs 

// 1. 
	// Start by getting the tags of the product 
	//http://stackoverflow.com/questions/31904758/woocommerce-get-product-tags-in-array
	$terms = get_the_terms( $post->ID, 'product_tag' );
	// echo '<pre>Terms';
	// print_r($terms);
	// echo '</pre>';

	// Use an array_filter to return FALSE if there are no values
	// This is used here more to clean the $arg below in step 2, 3
	// ref: https://stackoverflow.com/questions/2216052/how-to-check-whether-an-array-is-empty-using-php answer by James
	$terms_check = array_filter($terms);
	// echo '<pre>Array Filter';
	// print_r($terms_check);
	// echo '</pre>';
	
	// If there are terms and no errors, get the term slugs 
	if ( (isset($terms_check)) && ! is_wp_error( $terms_check ) ){
		// Put tags into array 
		foreach ( $terms_check as $term_check ) {
		    $terms_list[] = $term_check->slug;
		}
	}



// 2. Use product tags terms in a WP_Terms_Query to get posts with the same terms  
	// Query the site for other posts with matching terms  
	// WP_Query arguments
	$args = array(
		'post_type'              => array( 'post' ), // we want posts 
		'post_status'            => array( 'publish' ), // that are published 
		'nopaging'               => true, // get everything 
		'category_name'			 => 'latest', // with the category 'latest'
		'tag'					 => $terms_list, // and has the tags that we want
		);

	// The Term Query
	// we don't want to run the query at all if there are no tag values in $terms_list 
	// ref: https://stackoverflow.com/questions/8328983/check-whether-an-array-is-empty
		if (empty($terms_list) ) {
			return; //bail
		} else {
	    	// if the array has values then run the query 
	    	// This prevents products without a tag value from pulling all the IG posts 
			$product_tag_query = new WP_Query( $args );
		}
// 3. Get those post IDs 
	$post_ids = wp_list_pluck( $product_tag_query->posts, 'ID' );
	$post_ids_string = implode( ',', $post_ids );
	// echo '<pre>';
	// print_r($product_tag_query);
	// echo '</pre>';

	foreach ($post_ids as $postid) :
        $instagram_attachment_ids[]=get_post_thumbnail_id($postid);
    endforeach;

 //    echo '<pre>';
	// print_r($instagram_attachment_ids);
	// echo '</pre>';

// 4. 
	if ( $product_tag_query->have_posts() ) {
		while ( $product_tag_query->have_posts() ) {
			$product_tag_query->the_post();
			// merge the ids of the product_tag_query with the attachement IDs 
			// set a new variable to check for the gallery ids because we want to keep the WC query intact 
			$new_attachment_ids = $product->get_gallery_image_ids();
			
			// now merge these into the variable $attachment_ids 
			$attachment_ids = array_merge($new_attachment_ids, $instagram_attachment_ids);
		}
	} else {
		// no terms found, use the default WC logic 
		$attachment_ids = $product->get_gallery_image_ids();
	}

// Restore original Post Data
wp_reset_postdata();

// echo '<pre>product gallery IDs';
// print_r($new_attachment_ids);
// echo '</pre>';

// echo '<pre>';
// print_r($attachment_ids);
// echo '</pre>';

if ( $attachment_ids && has_post_thumbnail() ) {
		foreach ( $attachment_ids as $attachment_id ) {
		$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
		$thumbnail       = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
		$attributes      = array(
			'title'                   => get_post_field( 'post_title', $attachment_id ),
			'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
			'data-src'                => $full_size_image[0],
			'data-large_image'        => $full_size_image[0],
			'data-large_image_width'  => $full_size_image[1],
			'data-large_image_height' => $full_size_image[2],
		);

		$html  = '<div data-thumb="' . esc_url( $thumbnail[0] ) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '">';
		$html .= wp_get_attachment_image( $attachment_id, 'shop_single', false, $attributes );
 		$html .= '</a></div>';

		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
	}
}

