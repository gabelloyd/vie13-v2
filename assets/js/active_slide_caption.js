// Show captions from active slides on page
// This is for the shortcode "slidecaptions"

function get_active_slide_caption() {
	
		var active = $('.carousel-inner > .item.active > .slide-caption-data');
		var caption = active.text();

		// Write the text into the shortcode
		$( "span#slidecaption" ).html( caption );
}

function preload_next_image()
{
	var $itemActive = $('.carousel-inner').find('.item.active'),
		$itemActiveNext = $itemActive.next(),
		$fill = $itemActiveNext.find('.fill');

	// If we are not at the last slide, then we continue the function. 
	if($itemActiveNext.length !== 0) {
		var bg = $fill.css('background-image'),
		// Since this is a css background we must use regex to grab the actual url.
		src = bg.replace(/(^url\()|(\)$|[\"\'])/g, '');
		// We create an img and load it into cache.
		$('<img />').attr('src', src).on('load', function() {
			// Once loaded, we remove the tag to prevent memory leaks.
			$(this).remove();
		});
		//console.log(src + ' has been loaded');
		return;
	}
}

jQuery(document).ready(function($) {

	//Variables on page load 
	var myCarousel = $('#carousel-feature');
	var active = $('.carousel-inner > .item.active > .slide-caption-data');
	var caption = active.text();
	var	firstSlide = myCarousel.find('.item:first').find( caption );

	//Initialize carousel 
	myCarousel.carousel();

	//Show captions in first slide on page load 
	get_active_slide_caption(firstSlide);

	//This will load the following image on page load.
	preload_next_image();

	// Target Bootstrap to show this on 'slid' so it shows after a slide becomes active.
	myCarousel.on('slid.bs.carousel', function () {
		get_active_slide_caption();
		preload_next_image();
	});

});