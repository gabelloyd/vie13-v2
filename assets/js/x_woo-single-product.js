// Targets single product pages
// Going to set the columns on the page to the height of the browser minus the header

jQuery(document).ready(function($) {
	var pageHeader = $('#masthead').outerHeight(),
		breadcrumbs = $('.breadcrumb').outerHeight(),
		wpAdmin = $('#wpadminbar').outerHeight();
		

	if($('body').hasClass('logged-in')){
		var totalHeader = pageHeader + breadcrumbs + wpAdmin;
	} else {
		var totalHeader = pageHeader + breadcrumbs;
	}


	console.log("Page header is "+totalHeader+"px");
	
	// Set the height
	var imageColumn = $('.woocommerce-page div.product div.images'),
		contentColumn = $('.woocommerce-page div.product div.summary');

	if (Modernizr.mq('(min-width: 768px)')) {
		//imageColumn.css({'height': 'calc(100vh - '+totalHeader+'px)'});
		contentColumn.css({'height': 'calc(100vh - '+totalHeader+'px)'});
	}
	
	// Check your work
	var imageColumnHeight = imageColumn.outerHeight();
	console.log("Image Column is "+imageColumnHeight+"px");
	var contentColumnHeight = contentColumn.outerHeight();
	console.log("Content Column is "+contentColumnHeight+"px");
});