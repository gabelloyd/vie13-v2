/**
 * customizer.js
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );
	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-title a, .site-description' ).css( {
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				} );
			} else {
				$( '.site-title a, .site-description' ).css( {
					'clip': 'auto',
					'position': 'relative'
				} );
				$( '.site-title a, .site-description' ).css( {
					'color': to
				} );
			}
		} );
	} );

	// Custom Navbar Background Color
	wp.customize( 'nav_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '#site-navigation' ).css( {
				'background-color' : to
			});
			$( 'ul.sidebar_login_links' ).css( {
				'background-color' : to
			});
		} );
	} );
	// Custom Navbar Link Text Color
	wp.customize( 'nav_link_color', function( value ) {
		value.bind( function( to ) {
			$( '#site-navigation a' ).css( {
				'color' : to
			});
			$( 'ul.sidebar_login_links' ).css( {
				'color' : to
			});
		} );
	} );
	// Custom Navbar Link:hover Text Color
	wp.customize( 'nav_link_hf_color', function( value ) {
		value.bind( function( to ) {
			$( '#site-navigation a:hover' ).css( {
				'color' : to
			});
			$( '#site-navigation a:focus' ).css( {
				'color' : to
			});
			$( '#site-navigation a:active' ).css( {
				'color' : to
			});
			$( 'ul.sidebar_login_links' ).css( {
				'color' : to
			});
			$( '#site-navigation li.current-menu-item a' ).css( {
				'color' : to
			});
		} );
	} );
	// Custom Footer Background Color
	wp.customize( 'footer_bg_color', function( value ) {
		value.bind( function( to ) {
			$( '.site-footer' ).css( {
				'background-color' : to
			});
		} );
	} );
	// Custom Footer Text Color
	wp.customize( 'footer_text_color', function( value ) {
		value.bind( function( to ) {
			$( '.site-footer' ).css( {
				'color' : to
			});
		} );
	} );
	// Custom Footer Link Color
	wp.customize( 'footer_link_color', function( value ) {
		value.bind( function( to ) {
			$( '.site-footer a' ).css( {
				'color' : to
			});
		} );
	} );
	// Custom Footer Link:hover Color
	wp.customize( 'footer_link_hf_color', function( value ) {
		value.bind( function( to ) {
			$( '.site-footer a:hover' ).css( {
				'color' : to
			});
			$( '.site-footer a:focus' ).css( {
				'color' : to
			});
			$( '.site-footer a:active' ).css( {
				'color' : to
			});
		} );
	} );
} )( jQuery );
