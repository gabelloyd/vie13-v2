function onProgress( imgLoad, image ) {
  // change class if the image is loaded or broken
  var $item = $( image.img );
  // Make sure we appear our images 
  $item.removeClass('lazy').addClass('jetpack-lazy-image--handled appear');
  // Change the class for broken images 
  if ( !image.isLoaded ) {
    $item.removeClass('jetpack-lazy-image--handled appear').addClass('hidden'); // using hidden here because we use Bootstrap. Apply any class here that will remove from DOM or hide element
  }
}

jQuery(document).on('yith_infs_added_elem', function(){
	// make the images get a class of appear. 
	// triggered after each item is loaded
	var $container = $('ul.products');

	// use ImagesLoaded
	$container.imagesLoaded()
		.progress(onProgress);
});