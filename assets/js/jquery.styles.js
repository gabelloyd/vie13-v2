// Calculate #carousel-height so it is "full screen".
// #site-navigation wraps all header info.
// Carousel is position:relative; under #site-navigation
// Need to be able to set height of carousel to hit bottom of the browser window.

// we call the function after the document has loaded

function setCarouselHeight() {
	
	// This function sets #carousel-wrapper min-height

	// Start by checking all the header heights, including if we are logged in.
	var offsetTop				= $('#masthead').outerHeight(),
		wpadminbarHeight		= $('div#wpadminbar').outerHeight(),
		carouselHeight			= $(window).height() - offsetTop,
		overlayElementsHeight	= ($('#overlay-top').outerHeight()) + ($('#overlay-middle').outerHeight()) + ($('#overlay-bottom').outerHeight());

	//Modernizr Media Query
	// Only calc height if we are 768px or greater
	// If 767px or less, be height of content on page.
	// Then check the height of the overlay-content-wrapper and set #carousel-wrapper equal to that height
	var query = Modernizr.mq('(min-width: 600px)');

	
		// If we are logged in, then offset by the height of the admin-bar
		if ($('body').hasClass('logged-in')) {
			// Check if carouselHeight is less than overlayElementsHeight, set #carousel-wrapper to the greater height.
			if (carouselHeight > overlayElementsHeight){
				$('#carousel-wrapper').css("height", carouselHeight-wpadminbarHeight );
			} else {
				$('#carousel-wrapper').css("height", overlayElementsHeight-wpadminbarHeight );
			} // end check for carousel content heights
			// end check for admin-bar
		} else {
			if (carouselHeight > overlayElementsHeight){
				$('#carousel-wrapper').css("height", carouselHeight );
			} else {
				$('#carousel-wrapper').css("height", overlayElementsHeight );
			}
				
		}
	

}

function setCarouselNavMargin() {
	var size = parseInt($("a.carousel-control").css('font-size'));
	var marginTop = -(size/2);
	// console.log('nav font-size: '+size);
	// console.log('nav margin-top: '+marginTop);
	$('a.carousel-control').css({'margin-top': marginTop});
}

function setSecondaryNav() {

// Get the headers position from the top of the page, plus its own height
var offsetTopNav = $('#masthead').outerHeight();
var startY = offsetTopNav;

  if ($(window).scrollTop() > startY) {
  	
	$('.fixedDiv').slideDown();
	
  } else {
    $('.fixedDiv').slideUp();
  }
}

function unwrap_p_tags_from_images() {
	// ref: http://stackoverflow.com/questions/31527952/remove-p-tags-from-advanced-custom-fields-images
	// Images without an anchor link
	$('p > img').unwrap();
	// we will do this for images wrapped in an anchor
	//$('a > img').unwrap('p');
}

///////////////////////////////////////////////  Check if WooCommerce store wide notice is displayed.
// if the notice is displayed, move the footer up by the height of the notice
function demo_store_footer_style(){
	var noticeHeight = $('p.demo_store').outerHeight();

	if($("p.demo_store").length){
		$("footer.site-footer").css("margin-bottom", noticeHeight);
	} 

}

///////////////////////////////////////////////// Hide last two IG images if the window is 992px to 1199px
// Use jquery slice()


function hide_two_IG_images(){
	var gridItem = $(".grid > .grid-item");		

	if (Modernizr.mq('only all and (min-width: 992px) and (max-width: 1199px)')){
		gridItem.slice(-2).hide();
	} 
	if (Modernizr.mq('only all and (max-width: 991px)')){
		gridItem.css({'display' : 'block'});
	}
	if (Modernizr.mq('only all and (min-width: 1200px)')){
		gridItem.css({'display' : 'block'});
	}
}

///////////////////////////////////////////////  Sidebar login 

function build_sidebar_login_dropdown() {
	// LOGGED OUT STUFF
	// We have to move the login form outside the nesting of the navbar for the modal to work. Let's append it to the footer.
	$('#loginModal').appendTo('footer.site-footer');

	// Move any additional sidebar-login links to the modal
	$('body.logged-out ul.sidebar_login_links').appendTo('#loginModal .modal-content');

	// LOGGED IN STUFF

	// Add the classes we need to the navbar element
	// .dropdown to the list element with the dropdown
	$('body.logged-in li.widget_wp_sidebarlogin').addClass('dropdown');

	// .dropdown-menu to the stuff we want to be in the menu
	// add arialabelledby
	
	$('body.logged-in ul.sidebar_login_links').addClass('dropdown-menu').attr("aria-labelledby","loggedInWrapper");
	
	// Now we have to rearrange the HTML so the span wrapper is before the dropdown-menu
	$('.avatar_container').insertBefore('.sidebar_logged_in_title');
	$('<span class="caret"></span>').insertAfter('.sidebar_logged_in_title');

}

function run_sidebar_login_dropdown() {
	
	// This will make the dropdown fire on hover
	// ref: https://codepen.io/betdream/pen/frDqh
	// ref: https://codepen.io/alwaysbrightblue/pen/ezZEYr
	var parentD, childD, toggleD;

	parentD = $('ul.nav li.dropdown');
	toggleD = $('.dropdown-toggle');
	childD = $('.dropdown-menu');

	// Hover the menu item and fire the dropdown
	parentD.hover(function() {

	$(this).toggleClass('open').find(childD).stop(true, true).delay(200).fadeIn(500);
		toggleD.attr('aria-expanded', 'true');
	},
	function() {
		$(this).toggleClass('open').find(childD).stop(true, true).delay(200).fadeOut(500);
		toggleD.attr('aria-expanded', 'false');
	});

	// if the dropdown is open, we want to prevent it from refiring with a click
	parentD.on('shown.bs.dropdown', function(e) {

	e.stopPropagation();

	});

	// The open class is toggling on the parentD, so we force it to stay
	parentD.click(function(e){
		if ($(this).hasClass('open')){
		e.stopPropagation();
		toggleD.attr('aria-expanded', 'true');
	} else {
		$(this).addClass('open');
		toggleD.attr('aria-expanded', 'false');
		}
	});
	
}

///////////////////////////////////////////////// Testimonials 
function testimonial_slider_display(){
	$('.jetpack-testimonial-shortcode').flickity({
	  // options
	  cellAlign: 'left',
	  contain: true, // will contain cells to container so no excess scroll at beginning or end. has no effect if wrapAround is enabled
	  groupCells: true, // group cells together in slides
	  wrapAround: false // at end of cells, wraps-around to first for infinite scrolling
	});
}

/////////////////////////////////////////////////// Trigger BlockUI on buttons
// Button touch on mobile
// Using BlockUI.js library. It is already called with WooCommerce and Sidebar Login.
// TODO: Add conditional check if block is working
function buttonTouch(){
    
    $('.product-category a img, .button.wc-backward, .button.wc-forward, .button.product_type_simple, .button.single_add_to_cart_button').on('click touchstart', function() {
        var button = $(this);
        button.delay(50).queue(function(){
        	$(this).addClass('disabled').block({ 
        	message: null, 
        	overlayCSS: {
		        backgroundColor: '#fff',
		        opacity: 0.6,
		        padding: 20
		    }
		});
        });
	});
	
}

jQuery(document).ready(function($) {
	//console.log( "document loaded" );
	//On document ready, we run this first.
	buttonTouch();
	unwrap_p_tags_from_images();
	demo_store_footer_style();
	hide_two_IG_images();
	build_sidebar_login_dropdown();
	run_sidebar_login_dropdown();
	testimonial_slider_display();
	//_.debounce(toggleMainLogoforMobile(),100);
	_.debounce(setCarouselHeight(),150);
	_.debounce(setCarouselNavMargin(),200);

	// Do this on load just in case the user starts half way down the page
	setSecondaryNav();

	$('.product-search-results').attr("style", "width: 50%;");

	$(window).load(function() { //for page reload, we run this again
		
		//_.debounce(toggleMainLogoforMobile(),100);
		_.debounce(setCarouselHeight(),150);
		_.debounce(setCarouselNavMargin(),200);
	}); // end window.load

	// use underscores.js to delay the function until resize is done.
	$(window).resize(function() {
		hide_two_IG_images();
		//_.debounce(toggleMainLogoforMobile(),100);
		_.debounce(setCarouselHeight(),150);
		_.debounce(setCarouselNavMargin(),200);

	});

	$(window).scroll(function() {
		setSecondaryNav();
	});
});