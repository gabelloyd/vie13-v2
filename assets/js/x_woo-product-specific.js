// Woo products page

/*
     * Replace all SVG images with inline SVG
     * http://stackoverflow.com/questions/11978995/how-to-change-color-of-svg-image-using-css-jquery-svg-image-replacement
     */
// jQuery(document).ready(function($) {
//         jQuery('img.svg').each(function() {
//             var $img = jQuery(this);
//             var imgID = $img.attr('id');
//             var imgClass = $img.attr('class');
//             var imgURL = $img.attr('src');

//             jQuery.get(imgURL, function(data) {
//                 // Get the SVG tag, ignore the rest
//                 var $svg = jQuery(data).find('svg');

//                 // Add replaced image's ID to the new SVG
//                 if(typeof imgID !== 'undefined') {
//                     $svg = $svg.attr('id', imgID);
//                 }
//                 // Add replaced image's classes to the new SVG
//                 if(typeof imgClass !== 'undefined') {
//                     $svg = $svg.attr('class', imgClass+' replaced-svg');
//                 }

//                 // Remove any invalid XML tags as per http://validator.w3.org
//                 $svg = $svg.removeAttr('xmlns:a');

//                 // Replace image with new SVG
//                 $img.replaceWith($svg);

//             }, 'xml');

//         });
// });

// /* swap out svg for png on no-svg browsers */
// jQuery(document).ready(function($) {
//   if (!Modernizr.svg) {
//     jQuery(".svg img").attr("src", "<?php echo get_stylesheet_directory_uri(); ?>/img/very-basic-ruler.png");
//   }
// });


/* now we make the svg's container have the same height */
// jQuery(document).ready(function($)
// {
//      var objHeight = 0;

//      $.each($('#way-foot'), function(){
//             objHeight += $(this).height();

//      });
//      $('.svg-menu-link').height(objHeight).width(objHeight);
// });
// jQuery(document).ready(function($) {
//      var objHeight = 0;
//      var svgHeight = jQuery('.replaced-svg').outerHeight();
//      var svgWidth = jQuery('.replaced-svg').outerWidth();

//      $('.svg-menu-link').css('height', (objHeight + svgHeight) + 'px');
// });

// // stick the footer using Waypoints.js
// jQuery(document).ready(function($) {
// 	var sticky = new Waypoint.Sticky({
// 		element: $('#way-foot').first(),
// 		offset: 'bottom-in-view'
// 	});
// 	$(window).resize(wayFoot);
// 	wayFoot();
// });

// *
//  * using modernizr mq to determine of mobile or not
//  * if not mobile use waypoints for footer
//  * if mobile use velocity

// var wayFoot=function() {
// 	if (Modernizr.mq('(min-width: 768px)')) {
// 		var sticky = new Waypoint.Sticky({
// 			element: $('#way-foot').first(),
// 			offset: 'bottom-in-view'
// 		});
// 	} else {
// 		$('#way-foot').velocity("transition.slideUpIn", { display: 'block' }).addClass('stuck');
// 	}
// };
// jQuery(document).ready(function($) {

// 	if ($('#way-foot').length!=0) {
// 	  var sticky = new Waypoint.Sticky({
// 	    element: $('#way-foot').first(),
// 	    offset: 'bottom-in-view'
// 	  });
//   }

// });

// scrollTo links within footer
// jQuery(document).ready(function($) {

// 	$('#way-foot').localScroll({
// 		duration: 800,
// 	    offset: { 'left':0, 'top':-0.15*$(window).height() },
// 	    easing: 'easeOutQuad',
// 	    onAfter: function(target,settings) {
// 				//window.location.hash = target.attr('id'); //After smooth scrolling to anchor, add the hash to the browser address.
// 				if (history.pushState) {
// //console.log(target.attr('id'));
// 					history.pushState(null, null, '#'+target.attr('id'));
// 				}
// 				else {
// 					location.hash = target.attr('id');
// 				}

// 			}
// 	});

// });

// scrollTo Contact Us to bottom
// jQuery(document).ready(function($) {

//   $('#contact-btn').localScroll({
//     duration: 800,
//       offset: { 'left':0, 'top':-0.15*$(window).height() },
//       easing: 'easeOutQuad',
//       onAfter: function(target) {
//       location.hash = target.attr('id'); //After smooth scrolling to anchor, add the hash to the browser address.
//     }
//   });

//   $('#hero-btn').localScroll({
//     duration: 800,
//       offset: { 'left':0, 'top':-0.15*$(window).height() },
//       easing: 'easeOutQuad',
//       onAfter: function(target) {
//       location.hash = target.attr('id'); //After smooth scrolling to anchor, add the hash to the browser address.
//     }
//   });

// });

/* http://stackoverflow.com/questions/25935686/modernizr-media-query-doesn-t-work-when-resize-browser */
var mod = function(){
		//set variables to look at heights of DOM elements we want to target
		var outerHeight = jQuery(window).height();
		var navHeight = jQuery('#nav-bloc').outerHeight();
		var breadcrumbHeight = jQuery('.breadcrumb').outerHeight();
		var footerHeight = jQuery('.sticky-footer').outerHeight();
		var winW = jQuery(window).width();

           if (Modernizr.mq('(min-width: 768px)')) {
                //over 768 we look for heights of window, navbar, breadcrumb

				        jQuery('.woo-product-hero').css('height', (outerHeight - navHeight - breadcrumbHeight) + 'px'); // Set woo-product-hero to fill page height
				        jQuery('.flexslider').css('bottom', (15) + 'px'); // bump thumbnails above footer
                jQuery('.flexslider').css('width', (winW / 2) + 'px'); //have to give flexslider a width for slides to show.  http://stackoverflow.com/questions/14349167/flexslider-slide-width-issue
                jQuery('.woo-product-title-wrap').css('bottom', (footerHeight + 45) + 'px'); // bump woo-product-title-wrap above this

            } else {
                // under 768 we only look for height of window and navbar

                jQuery('.woo-product-hero').css('height', (outerHeight - navHeight) + 'px'); // Set woo-product-hero to fill page height
                jQuery('.flexslider').css('bottom', (5) + 'px'); // bump thumbnails above footer
                jQuery('.flexslider').css('width', (winW - 10) + 'px'); //have to give flexslider a width for slides to show.  http://stackoverflow.com/questions/14349167/flexslider-slide-width-issue
                jQuery('.woo-product-title-wrap').css('bottom', (footerHeight + 20) + 'px'); // bump woo-product-title-wrap above this
          }
        };

// Set product image height Scripts
jQuery(document).ready(function($) {
	// Call on every window resize
            $(window).resize(mod);
            // Call once on initial load
            mod();
	});


// thumbnails for product images
// http://flexslider.woothemes.com/dynamic-carousel-min-max.html

// jQuery(document).ready(function() {

//   // store the slider in a local variable
//   var $window = jQuery(window),
//       flexslider;

//   // tiny helper function to add breakpoints
//   function getGridSize() {
//   	//https://github.com/woothemes/FlexSlider/issues/646
//     return ( (window.innerWidth || document.documentElement.clientWidth) < 600) ? 5 : ( (window.innerWidth || document.documentElement.clientWidth) < 900) ? 6 : 7;
//   }

//   // jQuery(function() {
//   //   SyntaxHighlighter.all();
//   // });

//   $window.load(function() {
//     jQuery('.flexslider').flexslider({
//       animation: "slide",
//       animationLoop: false,
//       itemWidth: 50,
//       itemMargin: 5,
//       controlNav: false,
//       directionNav: false,
//       minItems: getGridSize(), // use function to pull in initial value
//       maxItems: getGridSize(), // use function to pull in initial value
//       start: function (slider) {
//         flexslider = slider; //Initializing flexslider here.
// 	    }
//     });
//   });

//   // check grid size on resize event
//   $window.resize(function() {
//     var gridSize = getGridSize();

// 	if (typeof flexslider !== 'undefined') {
// 	    flexslider.vars.minItems = gridSize;
// 		flexslider.vars.maxItems = gridSize;
// 	}
//   });
// }(jQuery));