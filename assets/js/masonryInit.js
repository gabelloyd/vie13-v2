// Masonry

jQuery(document).ready( function($) {
  // options Masonry
  var $grid = $('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    gutter: '.gutter-sizer',
    percentPosition: true
  });
  // layout Isotope after each image loads
  $grid.imagesLoaded()
    .progress( function() {
      $grid.masonry('layout');
    });
});