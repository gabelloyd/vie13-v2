// CALLED in functions.php 
// Targets just the woo category nav tabs
// Requires Bootstrap v3.3.7
// Requires Flicktiy
// To see the output, refer to /assets/functions/woocommerce/wc-category-nav-panel.php
// Updated February 2018

jQuery(document).ready(function($) {
	// Use Flicktiy to make this work
	// Need to delay the render of Flicktiy upon tab.active 
	// http://flickity.metafizzy.co/api.html#resize
	var wooCategoryTabList = $('.woo-category-tab-carousel').flickity({
	  // options
	  cellAlign: 'left',
	  contain: true, // will contain cells to container so no excess scroll at beginning or end. has no effect if wrapAround is enabled
	  groupCells: true, // group cells together in slides
	  wrapAround: false, // at end of cells, wraps-around to first for infinite scrolling
	  draggable: true
	});

	// For page load
	$('a[data-toggle="tab"]').on('show.bs.tab', function(){
		wooCategoryTabList.show()
			// trigger resize method after showing
			.flickity('resize');
		
	});

	// When a tab is active, trigger the flickity carousel 
	$('a[data-toggle="tab"]').on('shown.bs.tab', function(){
		wooCategoryTabList.show()
			// trigger resize method after showing
			.flickity('resize');
		
	});
	
	//////////////////////////////////////// Shift the nav tabs UP onto the carousel based on their height (allows text size change in CSS without needing to change this function)
	// We want to check the height of the tabs and offset by that amount.
	var wooTabWrapper = $('.woo-category-tab-wrapper'), // Get the wrapper class
		wooNavTabs = $('#catalog-navigation').outerHeight(); // Get the nav tab height
	
	wooTabWrapper.css({'margin-top': "-"+wooNavTabs+"px"}); //add a negative to pull it up.


	// Fire this again on window resize
	//https://css-tricks.com/snippets/jquery/done-resizing-event/
	var resizeTimer;

	$(window).on('resize', function(e) {

	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {
	  	wooTabWrapper.css({'margin-top': 0 }); //zero it out 
		wooTabWrapper.css({'margin-top': "-"+wooNavTabs+"px"}); //add a negative to pull it up.
	}, 250);

	});

});