function onProgress( imgLoad, image ) {
  // change class if the image is loaded or broken
  var $item = $( image.img );
  // Make sure we appear our images 
  $item.removeClass('lazy').addClass('appear');
  // Change the class for broken images 
  if ( !image.isLoaded ) {
    $item.removeClass('appear').addClass('hidden'); // using hidden here because we use Bootstrap. Apply any class here that will remove from DOM or hide element
  }
}

jQuery(document).ready(function($){

	//var postCount = $('#content article').length;
		//console.log(count);

	
	// https://gist.github.com/AlphaBlossom/845721ab5c556933de0e6092296944b5
	// Get max number of pages
	var maxpage = beloadmore.maxpage;
	//var maxpageval = maxpage.val();
	//console.log('maxpage value is '+maxpageval);
	//if(maxpage < 1) {
		$('body').addClass('infinite-scroll').addClass('neverending');	
		$('#main').append( '<span class="load-more"></span>' );		
	//}

	var button = $('.load-more');
	var page = 2;
	var loading = false;

	var scrollHandling = {
	    allow: true,
	    reallow: function() {
	        scrollHandling.allow = true;
	    },
	    delay: 225 //(milliseconds) adjust to the highest acceptable value
	};

	$(window).scroll(function(){
		if( ! loading && scrollHandling.allow ) {
			scrollHandling.allow = false;
			setTimeout(scrollHandling.reallow, scrollHandling.delay);
			var offset = $(button).offset().top - $(window).scrollTop();
			if( 1800 > offset ) {
				loading = true;
				$('.load-more').remove(); // remove the load more button
				$('#main').append( '<span class="woo-infinite-loader"></span>' );
				var data = {
					action: 'be_ajax_load_more',
					nonce: beloadmore.nonce,
					page: page,
					query: beloadmore.query,
				};
				$.post(beloadmore.url, data, function(res) {
					if( res.success) {
						// Add and Fade in new articles
						$(res.data).hide().appendTo('#main').fadeIn(1000);

						if( page >= maxpage) {
							// If last page, remove "load more" button and replace with "No more articles" text.
							$('#main .load-more').remove();
							$('body').removeClass('neverending'); // gets the footer back
							
						} else {
							$('#main').append( button );
							// make the images get a class of appear. 
							// triggered after each item is loaded
							var $container = $('#content');
							

							// use ImagesLoaded
							$container.imagesLoaded()
								.progress(onProgress);
						}
						page = page + 1;
						loading = false;
						$('.woo-infinite-loader').remove();
					} else {
						console.log(res);
					}
				}).fail(function(xhr, textStatus, e) {
					console.log(xhr.responseText);
				});

			}
		}
	});
});

