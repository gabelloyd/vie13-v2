$(document).ready(function(){
	var $imgs = $('#content img'),
		src;
	// Add class to make it easier to target images. 
	$imgs.addClass('lazy');
	// Looping through all images in content div.
	$imgs.each(function(){
		var $self = $(this),
			//Adding width, height as well as ratio.
			w = 1280,
			h = 720,  
			r = h/w;
		// Some images have srcset with multiple images. If that attribute exists we will use that.
		if($self.is('[srcset]')) {
			src = $self.attr('srcset');
		} else {
			src = $self.attr('src');
		}
		// We create a data attribute and put the src variable there. We also replace the 'src' attribute with preloader image and make 'srcset' to null. 
		$self.attr({
			'data-src': src,
			// You should replace this with a relative url. This is just here as a test.
			'src': '',
			'srcset': null
		});

		$self.on('load', function() {
			$self.addClass('appear');
		});
	});

	// on window resize, the height of the image should adjust accoringly.
	$(window).on('resize',function(){
		$('[data-ratio]').each(function(){
			var $img=$(this);
			$img.height( $img.width() * $img.attr('data-ratio') );
		});
	});
	
	// This is where Waypoint comes in.
	$('img.lazy').waypoint(function () {
		var $element = $(this.element);
		// We check to make sure the image has the lazy class.
		if($element.hasClass('lazy')) {
			$element.removeClass('appear');
			setTimeout(function(){
				$element.attr('srcset', $element.data('src'));
			}, 300);
			$element.on('load', function(){
				setTimeout(function(){
					$element.addClass('appear');
					// We remove the lazy class so the process doesn't repeat again.
					$element.removeClass('lazy');
				}, 300);
				//console.log('image loaded');
				var $grid = $('.grid').masonry({
					itemSelector: '.grid-item',
					columnWidth: '.grid-sizer',
					gutter: '.gutter-sizer',
					percentPosition: true,
					isAnimated: true
					//isFitWidth: true
				});
							// check if #msnry-container exists
			if ($('.grid').length > 0) {
			// layout Isotope after each image loads
			  $grid.imagesLoaded().progress( function() {
			    $grid.masonry();
			  });
			}
			});
		}
	},{
		offset: '100%'
	});

});
