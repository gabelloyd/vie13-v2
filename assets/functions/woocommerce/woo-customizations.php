<?php 

/** 
 * Change the Description tab link text for single products
 */
add_filter( 'woocommerce_product_description_heading', 'isa_wc_description_tab_link_text', 999, 2 );
 
function isa_wc_description_tab_link_text() {
 
    return null;
 
}

/** 
 * Change the heading on the Additional Information tab section title for single products.
 */
add_filter( 'woocommerce_product_additional_information_heading', 'isa_additional_info_heading' );
 
function isa_additional_info_heading() {
    return null;
}

/**
 *
 * Get the materials
 * @access public 
 * @return void 
 */

function v13_display_materials_attribute() {
	// Get the custom materials 
	$materials=wp_get_post_terms(get_the_ID(), 'material');
	if (count($materials)) : ?>
		<h2>Materials</h2>
		<ul class="woo-materials-list" layout="rows top-center">
			<?php foreach ($materials as $material) : ?>
				<li class="woo-materials-list-item">
					<?php
					$images_raw = get_option( 'taxonomy_image_plugin' );
					if ( array_key_exists( $material->term_id, $images_raw ) ) {
						echo wp_get_attachment_image( $images_raw[ $material->term_id ], 'shop_thumbnail',"", array( "class" => "woo-material-list-image" ) );
					} ?>
					<div class="woo-materials-list-text">
						<h4><?php echo $material->name; ?></h4>
						<?php if($material->description){ echo '<p>'.$material->description.'</p>';} ?>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif;
}

/**
 * 
 * Hide the SKUs on frontend but keep on admin side 
 * @access public 
 * @return void 
 * @since 1.0 
 * @author gabelloyd 
 **/

// https://www.skyverge.com/blog/how-to-hide-sku-woocommerce-product-pages/
function sv_remove_product_page_skus( $enabled ) {
    if ( ! is_admin() && is_product() ) {
        return false;
    }

    return $enabled;
}
add_filter( 'wc_product_sku_enabled', 'sv_remove_product_page_skus' );

/*
 * Change the breadcrumb separator
 * https://docs.woocommerce.com/document/customise-the-woocommerce-breadcrumb/#section-1
 */

add_filter( 'woocommerce_breadcrumb_defaults', 'v13_woocommerce_breadcrumbs' );
function v13_woocommerce_breadcrumbs() {
	return array(
		'delimiter'   => '',
		'wrap_before' => '<div class="full-width breadcrumb-bg"><div class="container"><ol class="breadcrumb">',
		'wrap_after'  => '</ol></div></div>',
		'before'      => '<li>',
		'after'       => '</li>',
		'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
	);
}

// To set the URL to include the Home and Shop base, refer to https://github.com/woocommerce/woocommerce/issues/1993

// Use this for Swapping the "home" or base link to the catalog. It's not perfect, but possibly useful 
// add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );
// function woo_custom_breadrumb_home_url() {
//     $baseURL = get_stylesheet_uri();
// 	$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
// 	$catalogURL = $baseURL.$shop_page_url;
//     return $catalogURL;
// }

/**
 * woocommerce_category_featured_image function  
 *
 * Give me a category thumbnail image. Used in the archive page as the header 
 * https://docs.woocommerce.com/document/woocommerce-display-category-image-on-category-archive/
 * http://wordpress.stackexchange.com/questions/200170/woocommerce-get-category-image-full-size
 * 
 * Out put a header image for the categories with a fallback to a featured image. 
 * Featured image seems to only be necessary for the /shop base page. 
 * 
 * @access public
 * @author gabelloyd
 * @since 1.0
 * @return void 
 */

// check for plugin using plugin name
function set_image_size() {

	/**
	 * Detect plugin. For use on Front End only.
	 */
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if ( is_plugin_active( 'mobble/mobble.php' ) ) {

		if(is_mobile()){
			$size = 'large';
		} elseif (is_tablet()) {
			$size = 'fixed-height-gallery';
		} else {
			$size = 'full';
		}

		return $size;
	} else {
		$size = 'full';
	}
}

// This is output on the archive-product.php line 37
function woocommerce_category_featured_image() {


  
    if ( is_product_category() ){
	    global $wp_query;
	    
		// Category Image 
		$size = set_image_size();
	    // get the query object
		$cat = $wp_query->get_queried_object();
		// get the thumbnail id using the queried category term_id
		$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
		// Get the attachment image information. Set the thumbnail size you want here.
		$category_img = wp_get_attachment_image_src( $thumbnail_id, $size, false );

		if ( $category_img ) {
		    echo 'style="background-image: url(' . $category_img[0] . ');"';
		}
		wp_reset_postdata();

	} elseif (is_shop()) {
		// Help from https://gist.github.com/Bradley-D/7287723
		// Featured Image 
		// get the shop ID
		$post_id = get_option( 'woocommerce_shop_page_id' );
		// Get the size we want 
		$size = set_image_size();
		// Set the url 
    	$featured_image = get_the_post_thumbnail_url( $post_id, $size );;
		if ( $featured_image ) {
			echo 'style="background-image: url(' . $featured_image . ');"';
		}
		// wp_reset_postdata();

	}
}

/**
 * v13_single_product_attributes function.
 *
 * @access public
 * @param bool $attr_id (default: false)
 * @param string $title (default: '')
 * @param string $classes (default: '')
 * @return void
 */
function v13_single_product_attributes($attr_id=false,$title=false,$classes='') {
	global $product;

	$html=null;
	$attributes = $product->get_attributes();

	if (!$attr_id)
		return false;

	if (!isset($attributes[$attr_id]))
		return false;

	$attribute=$attributes[$attr_id];

	$terms=get_terms($attribute['name']);

	if (!$terms || is_wp_error($terms))
		return false;

	if ( $attribute['is_taxonomy'] ) {
		$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
	} else {
		// Convert pipes to commas and display values
		$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
	}

	if (!$title)
		$title=wc_attribute_label( $attribute['name'] );

	$html.='<h3 class="text-center">'.$title.'</h3>';

	$html.='<ul class="'.$classes.' '.$attr_id.'">';
		foreach ($values as $value) :
			$html.='<li>'.$value.'</li>';
		endforeach;
	$html.='</ul>';

	echo $html;
}

/**
 * v13_single_product_size_chart function.
 *
 * @access public
 * @param bool $post_id (default: false)
 * @return void
 */
function v13_single_product_size_chart($post_id=false) {

	// Check if WooCommerce is active //
	if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) )
		return false;

	// check for table press //
	if (!get_option('tablepress_tables'))
		return false;
	
	// Get the post ID 
	$post_id = get_the_ID();

	// if there isn't a post ID, bail 
	if (!$post_id || !get_field('sizing_table',$post_id))
		return false;

	$tablepress_tables=json_decode(get_option('tablepress_tables'));
	$sizing_table=get_field('sizing_table',$post_id);
	$size_table_ids=array();
	$table_ids=array();

	foreach ($sizing_table as $s_table) :
		$size_table_ids[]=$s_table->ID;
	endforeach;

	if (empty($size_table_ids))
		return false;

	foreach ($tablepress_tables->table_post as $this_table_id => $table_post_id) :
		if (in_array($table_post_id,$size_table_ids))
			$table_ids[]=$this_table_id;
	endforeach;

	if (empty($table_ids))
		return false;

	foreach ($table_ids as $table_id) :
		if (do_shortcode('[table id='.$table_id.' /]'))
			echo do_shortcode('[table id='.$table_id.' /]');
	endforeach;
}

/**
 * v13_wc_custom_product_cat_link function.
 *
 * @access public
 * @param mixed $category
 * @return void
 */
function v13_wc_custom_product_cat_link($category) {
	$link=get_term_link( $category->slug, 'product_cat' );

	$args=array(
		'post_type' => 'product',
		'product_cat' => $category->slug,

	);
	$posts=get_posts($args);

	if (count($posts)==1) :
		$link=get_permalink($posts[0]->ID);
	endif;

	return $link;
}

/**
 * v13_get_size_table function.
 *
 * @access public
 * @param bool $id (default: false)
 * @return void
 */
function v13_get_size_table($id=false) {
	// check for table press //
	if (!get_option('tablepress_tables'))
		return false;

	$html=null;
	$tablepress_tables=json_decode(get_option('tablepress_tables'));
	$tables=$tablepress_tables->table_post;

	if ($id) :
		foreach ($tables as $table_id => $post_id) :
			if ($table_id==$id && do_shortcode('[table id='.$table_id.' /]'))
				$html.=do_shortcode('[table id='.$table_id.' /]');
		endforeach;
	else :
		foreach ($tables as $table_id => $post_id) :
			if (do_shortcode('[table id='.$table_id.' /]'))
				$html.=do_shortcode('[table id='.$table_id.' /]');
		endforeach;
	endif;

	return $html;
}

/**
 * v13_wc_no_sale_add_to_cart function
 * Hide the add to cart button if the category has the term 'shop' somewhere. 
 * this will hide the Add to Cart and Quantity inputs for all categories except the "Shop" and it's children  
 *
 * @access public
 * @return void
 */

function v13_wc_no_sale_add_to_cart() {
	// If the checkbox is active, remove_action for add o cart button on pages 
	global $post;

	if ( ! is_product() ) return;

	if (!has_term('shop', 'product_cat')) :
		remove_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
		remove_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 );
		remove_action( 'woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 30 );
		remove_action( 'woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30 );

		add_action( 'woocommerce_simple_add_to_cart', 'v13_no_sale_text', 30 );
		add_action( 'woocommerce_grouped_add_to_cart', 'v13_no_sale_text', 30 );
		add_action( 'woocommerce_variable_add_to_cart', 'v13_no_sale_text', 30 );
		add_action( 'woocommerce_external_add_to_cart', 'v13_no_sale_text', 30 );
	endif;

}
add_action('wp','v13_wc_no_sale_add_to_cart');

/**
 * v13_no_sale_text function
 * return null to do nothing 
 *
 * @access public
 * @return void
 */
function v13_no_sale_text() {
	return null;
}

/**
 * Change the add to cart text 
 *
 * @access public 
 * @return void 
 * @author @gabelloyd 
 * @since 1.0
 */

add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
/**
 * custom_woocommerce_template_loop_add_to_cart
*/
function custom_woocommerce_product_add_to_cart_text() {
	global $product;
	
	$product_type = $product->product_type;
	
	switch ( $product_type ) {
		case 'external':
			return __( 'Buy product', 'woocommerce' );
		break;
		case 'grouped':
			return __( 'View products', 'woocommerce' );
		break;
		case 'simple':
			return __( 'View Details', 'woocommerce' );
		break;
		case 'variable':
			return __( 'Select options', 'woocommerce' );
		break;
		default:
			return __( 'View Details', 'woocommerce' );
	}
	
}

/**
 * Add previous and next links to products under the product details 
 *
 * @access public 
 * @return void 
 * @author @kloon 
 * @since 1.0
 */
// https://gist.github.com/kloon/6493114
// https://businessbloomer.com/woocommerce-add-nextprevious-single-product-page/
// https://wordpress.stackexchange.com/questions/256505/next-and-prev-product-from-only-current-sub-category
// add_action( 'woocommerce_single_product_summary', 'wc_next_prev_products_links', 60 );
// function wc_next_prev_products_links() {
//     previous_post_link( '%link', '&larr; PREVIOUS', TRUE, ' ', 'product_cat' );
// 	echo ' | ';
// 	next_post_link( '%link', 'NEXT &rarr;', TRUE, ' ', 'product_cat');
// }


/**
* Change default thumbnail 
* @since 1.0
* @author Gabe Lloyd 
* ref: https://core.trac.wordpress.org/ticket/25449
*/
add_action( 'init', 'custom_fix_thumbnail' );
 
function custom_fix_thumbnail() {
  add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');
   
	function custom_woocommerce_placeholder_img_src( $src ) {
	$upload_dir = wp_upload_dir();
	$baseurl = untrailingslashit( $upload_dir['baseurl'] );
	$set_url_scheme = set_url_scheme( $baseurl );
	$src = $set_url_scheme . '/2017/04/coming-soon.png';
	 
	return $src;
	}
}

/**
* Remove Related Products 
* @since 1.0
* @author Gabe Lloyd 
* ref: https://docs.woocommerce.com/document/remove-related-posts-output/
*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/**
* Remove Sorting and Results count 
* @since 1.0
* @author Gabe Lloyd 
* ref: https://hirejordansmith.com/how-to-remove-the-woocommerce-results-count/
*/
// Remove the sorting dropdown from Woocommerce
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30 );
// Remove the result count from WooCommerce
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
