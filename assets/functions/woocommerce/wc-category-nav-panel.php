<?php 

add_filter('woocommerce_get_catalog_ordering_args', 'custom_woocommerce_catalog_orderby');
function custom_woocommerce_catalog_orderby( $args ) {
    $args['orderby'] = 'menu_order';
    $args['order'] = 'asc'; 
    return $args;
}

function get_front_page_tab_panels($limit=5) {

	$thumbnail_size = 'medium-square';

	$html=null;
	$product_terms_counter=1;
	$term_counter=1;
	$terms_args=array(
		'parent' => 0,
	
	);
	$product_terms=get_terms('product_cat',$terms_args);

	if (empty($product_terms))
		return false;
	/* We use the plugin 'mobble' to sniff for media queries in php. https://wordpress.org/plugins/mobble/ 
	 * We are also going to fallback to media queries within the functions for browser resizing
	 * If it's small, use Bootstrap List Group http://getbootstrap.com/components/#list-group-linked
	 * If it's equal to or greater than medium, use Bootstrap Tab Group http://getbootstrap.com/javascript/#tabs
	*/

	if(!is_mobile()){
	
	$html.='<div class="woo-category-tab-wrapper hidden-xs">'; // use jquery to move the tabs up onto the carousel with a negative margin-top 
		$html.='<div class="category-nav" role="tabpanel">';
		$html.='<!-- Nav tabs -->';
			$html.='<ul class="nav nav-tabs tabs-angled" role="tablist" id="catalog-navigation" layout="row center-stretch">';
				foreach ($product_terms as $term) :
					if ($term_counter==1) :
						$class='active';
					else :
						$class=null;
					endif;

					$name=$term->name;

					$html.='<li role="presentation" class="'.$class.'" data-catalog-page="'.get_term_link($term).'"><a href="#'.$term->slug.'" aria-controls="'.$term->slug.'" role="tab" data-toggle="tab">'.$name.'</a></li>';
					$term_counter++;
				endforeach;
			$html.='</ul>';
		$html.='</div><!-- /.tabpanel -->';

		$html.='<!-- Tab panes -->';

		$html.='<div class="tab-content">';

			foreach ($product_terms as $term) :
				$product_sub_terms_counter=1;
				$terms_args=array(
					'parent' => $term->term_id,
					
				);
				$product_sub_terms=get_terms('product_cat',$terms_args);

				if (count($product_sub_terms)>$limit) :
					$paneClass='show';
				else :
					$paneClass='';
				endif;

				if ($product_terms_counter==1) :
					$class='active';
				else :
					$class=null;
				endif;


				$html.='<div role="tabpanel" class="tab-pane fade in '.$class.'" id="'.$term->slug.'">';

					$html.='<ul class="woo-category-tab-carousel tab-list list-inline">';
						foreach ($product_sub_terms as $sub_term) :
							$classes=array();
							$thumbnail_id=get_woocommerce_term_meta($sub_term->term_id,'thumbnail_id',true);

							if ($thumbnail_id) :
								$category_image=wp_get_attachment_image($thumbnail_id, $thumbnail_size ,false,array('class' => 'img-responsive'));
							else :
								$dimensions=v13_get_image_size($thumbnail_size);
								$category_image='<img src="'.wc_placeholder_img_src().'" alt="Placeholder" width="'.$dimensions['width'].'" height="'.$dimensions['height'].'" class="woocommerce-placeholder wp-post-image" />';
							endif;

							$classes[]=$sub_term->slug;

							$html.='<li class="tab-cat-thumb '.implode(' ',$classes).'" >';
								$html.='<a href="'.get_term_link($sub_term).'" class="">';
									$html.='<h3 class="cat-title tab-content-title">'.$sub_term->name.'</h3>';
									
									$html.=$category_image;
								$html.='</a>';
							$html.='</li>';

							$product_sub_terms_counter++;
						endforeach;
					$html.='</ul><!-- /.tab-list -->';


				$html.='</div><!-- .tab-pane -->';

				$product_terms_counter++;

			endforeach;

		$html.='</div><!-- .tab-content -->';
	$html.='</div><!-- .woo-category-tab-wrapper -->';


	// Fall back for browser resizing
	$html.='<!-- List Group -->';
	$html.='<div class="woo-category-list-wrapper list-group hidden-sm hidden-md hidden-lg">';
	
		foreach ($product_terms as $term) :
			$name=$term->name;
			$html.='<a href="'.get_term_link($term).'" class="list-group-item cat-title" id="'.$term->slug.'">'.$name.'<span class="glyphicon glyphicon-chevron-right"></span></a>';
			$term_counter++;
		endforeach;
		
	$html.='</div><!-- /.list-group -->';

} else {
	$html.='<!-- List Group -->';
	$html.='<div class="woo-category-list-wrapper list-group">';
	
		foreach ($product_terms as $term) :
			$name=$term->name;
			$html.='<a href="'.get_term_link($term).'" class="list-group-item cat-title" id="'.$term->slug.'">'.$name.'<span class="glyphicon glyphicon-chevron-right"></span></a>';
			$term_counter++;
		endforeach;
		
	$html.='</div><!-- /.list-group -->';
}

	
	

	return $html;
}

/**
 * v13_get_category_thumbnail function.
 *
 * @access public
 * @param bool $category (default: false)
 * @param string $size (default: 'catalog-image')
 * @param int $placeholder_width (default: 0)
 * @param int $placeholder_height (default: 0)
 * @return void
 */
function v13_get_category_thumbnail($category=false,$size='medium-square',$placeholder_width=0,$placeholder_height=0) {
	if ($category) :
		$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
	else :
		$thumbnail_id=false;
	endif;

	if ($thumbnail_id) :
		$image=wp_get_attachment_image_src($thumbnail_id, $size, false);
		$image=$image[0];
	else :
		$image=wc_placeholder_img_src();
	endif;

	return '<img src="'.$image.'" alt="'.trim(strip_tags( get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true) )).'" class="img-responsive attachment-'.$size.'">';
}

/**
 * v13_get_image_size function.
 *
 * @access public
 * @param mixed $name
 * @return void
 */
function v13_get_image_size( $name ) {
	global $_wp_additional_image_sizes;

	if ( isset( $_wp_additional_image_sizes[$name] ) )
		return $_wp_additional_image_sizes[$name];

	return false;
}


// Instagrate Pro Shortcode
function wc_cat_tabs_shortcode($atts) {

	$atts=shortcode_atts(array(
		'limit' 			=> 8, // number of posts
		
	),$atts);

	ob_start();
	return get_front_page_tab_panels($atts['limit']);
	return ob_get_clean();
}
add_shortcode( 'wc_cat_tabs', 'wc_cat_tabs_shortcode' );