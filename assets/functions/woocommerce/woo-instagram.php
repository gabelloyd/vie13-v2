<?php 

/**
 * woo_is_instagram_tag_product_tag function.
 *
 * @access public
 * @param mixed $post_id
 * @return true/false $product_ids_return if true, returns product ids (array)
 */
function woo_is_instagram_tag_product_tag($post_id) {
	$post_tags=wp_get_post_tags($post_id);
	$post_tags_short=array();
	$product_terms=get_terms('product_tag');
	$product_terms_clean=array();
	$product_ids_return=false;

	// creates a simple array of slugs for easy comparison //
	foreach ($post_tags as $tag) :
		$post_tags_short[]=$tag->slug;
	endforeach;

	// create an array of product tags and product ids //
	foreach ($product_terms as $product_term) :
		$product_ids=array();
		$product_args=array(
			'posts_per_page' => -1,
			'post_type' => 'product',
			'product_tag' => $product_term->slug
		);
		$products=get_posts($product_args);

		if (count($products)) :
			foreach ($products as $product) :
				$product_ids[]=$product->ID;
			endforeach;
			$product_terms_clean[$product_term->slug]=$product_ids;
		endif;

	endforeach;

	// cycle through and look for matches, if match, send back link //
	foreach ($post_tags_short as $post_tag) :
		foreach ($product_terms_clean as $product_tag => $product_ids) :
			if ($post_tag==$product_tag) :
				foreach ($product_ids as $product_id) :
					$product_ids_return[]=$product_id;
				endforeach;
			endif;
		endforeach;
	endforeach;

	return $product_ids_return;
}

/**
 * woo_get_product_instagram_photos function.
 *
 * @access public
 * @return void
 * currently not in use
 */
function woo_get_product_instagram_photos() {
	global $post;

	$html=null;
	$limit=-1;
	$product_tags=wp_get_post_terms($post->ID,'product_tag');
	$product_tags_short=array();
	$instagram_post_ids=array();

	$instagram_args=array(
		'posts_per_page' => $limit,
		'category_name' => 'latest'
	);
	$instagram_posts=get_posts($args);

	if (!count($product_tags))
		return false;

	// creates a simple array of slugs for easy comparison //
	foreach ($product_tags as $tag) :
		$product_tags_short[]=$tag->slug;
	endforeach;

	foreach ($instagram_posts as $instagram_post) :
		$post_tags=wp_get_post_tags($instagram_post->ID);

		foreach ($post_tags as $tag) :
			if (in_array($tag->slug,$product_tags_short)) :
				$instagram_post_ids[]=$instagram_post->ID;
				break;
			endif;
		endforeach;

	endforeach;

	if (!count($instagram_post_ids))
		return false;

	$html.='<div class="product-gallery">';
		$html.='<h2>gallery</h2>';

		foreach ($instagram_post_ids as $post_id) :
			if (has_post_thumbnail($post_id)) :
				$html.='<div class="col-md-3">';
					$html.='<a href="'.get_permalink($post_id).'">';
						$html.=get_the_post_thumbnail($post_id,'', array('class' => 'img-responsive'));
					$html.='</a>';
				$html.='</div>';
			endif;
		endforeach;

	$html.='</div><!-- .product-gallery -->';

	echo $html;
}



/**
 * get_product_instagram_photo_ids function.
 *
 * This function gets the attachement ids of any post that has a tag that matches the product_tag for the product bein viewed. 
 * We store the attachment IDs in an array. 
 * We retrieve the array in the WooCommerce template product-thumbnails.php
 * The intended output is to see the attachment images for the product as well as the attachement images for any post with the same tag.
 * Example: Viewing product "Jersey" with a tag of #jersey, we find all posts with a tag of #jersey and display those images on the product page "Jersey".
 *
 * @access public
 * @param bool $post_id (default: false)
 * @return void
 */
function get_product_instagram_photo_ids($post_id=false) {
	if (!$post_id){
		return false;
	}

	$limit=-1;
	
	// Start by getting the tags of the product 
	$terms = get_terms('product_tag'); //http://stackoverflow.com/questions/31904758/woocommerce-get-product-tags-in-array
	// Put tags into array 
	$term_array = array();
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
	    foreach ( $terms as $term ) {
	        $term_array[] = $term->name;
	    }
	}

	// Now get posts with the same term 


	$product_tags_short=array();
	$instagram_post_ids=array();
	$instagram_attachment_ids=array();

	$instagram_args=array(
		'posts_per_page' => $limit,
		'category_name' => 'latest'
	);
	$instagram_posts=get_posts($instagram_args);

	if (!count($product_tags)){
		return false;
	}

	// creates a simple array of slugs for easy comparison //
	foreach ($product_tags as $tag) :
		$product_tags_short[]=$tag->slug;
	endforeach;

	foreach ($instagram_posts as $instagram_post) :
		$post_tags=wp_get_post_tags($instagram_post->ID);

		foreach ($post_tags as $tag) :
			if (in_array($tag->slug,$product_tags_short)) :
				$instagram_post_ids[]=$instagram_post->ID;
				break;
			endif;
		endforeach;

	endforeach;

	if (!count($instagram_post_ids)){
		return false;
	}

	foreach ($instagram_post_ids as $postid) :
		$instagram_attachment_ids[]=get_post_thumbnail_id($postid);
	endforeach;

	return $instagram_attachment_ids;

	echo $product_tags;
}


/**
 * v13_wc_product_thumbnails function.
 *
 * @access public
 * @param int $max_thumbs (default: 5)
 * @return void
 */
// function v13_wc_product_thumbnails($max_thumbs=5) {
// 	global $post, $product, $woocommerce;

// 	$html=null;
// 	$thumb_counter=1;
// 	$inst_ids=array();
// 	$attachment_ids = $product->get_gallery_attachment_ids();

// 	if ( !$attachment_ids )
// 		return false;

// 	if (count($attachment_ids)<$max_thumbs) :
// 		$inst_ids= get_product_instagram_photo_ids($post->ID);
// 		$attachment_ids=array_merge($attachment_ids,$inst_ids);
// 	endif;

// 	$html.='<div class="flexslider" id="carousel">';
// 		$html.='<ul class="slides">'; // att instagram images that match the correct tag here, too.  Prioritize the thumbnails added in the woocommerce admin area, but supplement with instagram

// 			foreach ( $attachment_ids as $attachment_id ) :
// 				$classes = array( 'zoom' );

// 				$image_link = wp_get_attachment_url( $attachment_id );

// 				if ( ! $image_link )
// 					continue;

// 				$image = wp_get_attachment_image( $attachment_id, 'catalog-single-product-gallery-thumb');
// 				$image_class = esc_attr( implode( ' ', $classes ) );
// 				$image_title = esc_attr( get_the_title( $attachment_id ) );

// 				$html.='<li>';
// 					$html.='<a href="'.$image_link.'" class="'.$image_class.'" title="'.$image_title.'" data-rel="prettyPhoto[product-gallery]">';
// 						$html.=$image;
// 					$html.='</a>';
// 				$html.='</li>';

// 				if ($thumb_counter==$max_thumbs)
// 					break;

// 			endforeach;

// 		$html.='</ul>';
// 	$html.='</div><!-- .flexslider -->';

// 	echo $html;
// }