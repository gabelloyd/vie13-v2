<?php 

/**
 * All logic for creating ids for sections based on menu titles declared in admin with ACF fields 
 * @since 1.1
 * @author gabelloyd erikdmitchell 
 *
 **/

/**
 *
 * Single Page Framework Navigation
 * @package Single_Page_Framework
 * @since 1.0.0
 **/

/*
function spf_content_nav_body_classes($classes) {
	return $classes;
}
add_filter('body_class', 'spf_content_nav_body_classes');
*/

/**
 * title_to_anchor_link function.
 *
 * @access public
 * the bootstrap navigation relies on matching anchor links
 * this function takes the title/subtitle and turns them into anchor links
 */

function title_to_anchor_link($title,$prefix='#') {
	$link = $title;
	$unwantedChars = array(",", "!", "?", "'"); // create array with unwanted chars
	$link = str_replace($unwantedChars, '', $link); // remove them
	$link = strtolower($link); //convert to lowercase
	$link = trim(preg_replace('/\W+/', '-', $link)); //replace spaces and other punctuation with spaces, then trim it
	$link = str_replace(' +','-',$link); //and spaces are replaced with spaces. Double spaces are removed.
	$link = $prefix.$link;

	return $link;
}


/**
 * title_to_id function.
 *
 * @access public
 * @param string $title (default: '')
 * @return void
 */
function title_to_id($title='') {
	$id = $title;
	$unwantedChars = array(",", "!", "?", "'"); // create array with unwanted chars
	$id = str_replace($unwantedChars, '', $id); // remove them
	$id = strtolower($id); //convert to lowercase
	$id = trim(preg_replace('/\W+/', '-', $id)); //replace spaces and other punctuation with spaces, then trim it
	$id = str_replace(' +','-',$id); //and spaces are replaced with spaces. Double spaces are removed.

	return $id;
}

/**
 * spf_content_navigation function.
 *
 * @access public
 * @param int $post_id (default: 0)
 * @return void
 */
function spf_content_navigation($post_id=0) {
	global $post;

	// set to current post if not passed //
	if (!$post_id)
		$post_id=$post->ID;

	$html=null;
	$section_titles=spf_get_content_section_titles($post_id);

	$html.='<ul id="section-content-menu" class="nav navbar-nav nav-menu section-content-nav">';
		foreach ($section_titles as $title) :

			// make sure we have title //
			if (!$title || $title=='')
				continue;

			$html.='<li class="nav-item"><a href="'.title_to_anchor_link($title).'" class="menu-link">'.$title.'</a></li>';
		endforeach;
	$html.='</ul>';

	echo $html;
}

/**
 * spf_get_content_section_titles function.
 *
 * @access public
 * @param int $post_id (default: 0)
 * @return void
 */
function spf_get_content_section_titles($post_id=0) {
	$section_meta_suffix='menu_display_name_v2';
	$sections=spf_get_menu_sections($post_id);
	$section_titles=array();

	// get our section ids //
	foreach ($sections as $section) :
		$section_titles[]=get_post_meta($post_id, $section.$section_meta_suffix, true);
	endforeach;

	return $section_titles;
}

/**
 * spf_get_menu_sections function.
 *
 * @access public
 * @param int $post_id (default: 0)
 * @return void
 */
function spf_get_menu_sections($post_id=0) {
	$sections=array();
	$meta=get_post_meta($post_id);
	$display_sections_in_menu=array();

	// get all sections (via meta) to display in menu //
	foreach ($meta as $key => $m_arr) :
		$pos=strpos($key, 'display_section_in_menu_v2');

		if ($pos!==false) :
			// strip off a potential _ b/c it meta //
			$display_sections_in_menu[]=ltrim($key, '_');
		endif;
	endforeach;

	$display_sections_in_menu=array_unique($display_sections_in_menu); // remove dups
	$display_sections_in_menu=array_values($display_sections_in_menu); // reset keys

	// get the section prefixes and make sure we're supposed to show it //
	foreach ($display_sections_in_menu as $key => $section_in_menu) :
		if (get_post_meta($post_id, $section_in_menu, true))
			$sections[]=str_replace('display_section_in_menu_v2', '', $section_in_menu);
	endforeach;

	return $sections;
}