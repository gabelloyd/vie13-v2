<?php
///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////  ACF CUSTOMIZATIONS  ////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

// Add theme options page
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

//move the content of a custom field to the_content() automatically
//ref: https://wordpress.org/support/topic/move-the-content-of-a-custom-field-to-the_content-automatically?replies=4
// function add_my_custom_field($data) {
//     $data .= esc_html( get_sub_field('text_editor_region'));
//     return $data;
// }
// add_filter('the_content','add_my_custom_field');


//remove_filter ('acf_the_content', 'wpautop'); // this is for advanced custom fields WYSIWIG

add_filter('acf_the_content', 'remove_acf_empty_p', 20, 1);
function remove_acf_empty_p($content){
    $content = force_balance_tags($content);
    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}
// http://wordpress.stackexchange.com/questions/13798/remove-empty-paragraphs-from-the-content

// remove p tags from images for ACF content areas
// https://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/
function filter_ptags_on_acf_images($acf_content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $acf_content);
}

add_filter('acf_the_content', 'filter_ptags_on_acf_images');

?>