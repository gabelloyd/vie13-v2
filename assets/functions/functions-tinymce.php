<?php
////////////////////////////////////////////////////////////
////////////////// WP EDITOR CUSTOMIZATIONS  ///////////////
////////////////////////////////////////////////////////////


/**
 *
 * Add Formats to Visual Content Editor
 * https://codex.wordpress.org/TinyMCE_Custom_Styles
 */

function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'hwt-artz',
			'selector' => 'span, h1, h2, h3, h4, h5, h6, p',
			'classes' => 'font-heading',
		),
		array(
			'title' => 'Adelle',
			'selector' => 'span, h1, h2, h3, h4, h5, h6, p',
			'classes' => 'font-main',
		),
		array(
			'title' => 'Big Font',
			'selector' => 'span, h1, h2, h3, h4, h5, h6, p',
			'classes' => 'big',
		),
		array(
			'title' => 'Extend Half',
			'block' => 'figure',
			'classes' => 'half-width-image',
			'wrapper' => true
		),
		array(
			'title' => 'Extend Full',
			'block' => 'figure',
			'classes' => 'full-width-image',
			'wrapper' => true
		),
		array(
			'title' => 'Remove Bottom Border',
			'selector' => 'div, span, h1, h2, h3, h4, h5, h6, p, img',
			'classes' => 'no-border-bottom',
		),
		array(
			'title' => 'Add Bottom Border',
			'selector' => 'div, span, h1, h2, h3, h4, h5, h6, p, img',
			'classes' => 'border-bottom',
		),

		array(
			'title' => '.bg-fade-dark',
			'selector' => 'div, span, h1, h2, h3, h4, h5, h6, p, img',
			'classes' => 'bg-fade-dark',
		),
		array(
			'title' => 'Hightlight',
			'block' => 'span',
			'classes' => 'highlight highlight--wrapping',
			'wrapper' => true,
		),
		array(
			'title' => 'Default Button',
			'block' => 'a',
			'classes' => 'btn btn-danger',
			'wrapper' => true
		),
		array(
			'title' => 'Default Button LG',
			'block' => 'a',
			'classes' => 'btn btn-danger btn-lg',
			'wrapper' => true,
		),
		array(
			'title' => 'Default Button XL',
			'block' => 'a',
			'classes' => 'btn btn-danger btn-xl',
			'wrapper' => true,
		),
		array(
			'title' => 'Default Button SM',
			'block' => 'a',
			'classes' => 'btn btn-danger btn-sm',
			'wrapper' => true,
		),
		array(
			'title' => 'Wire Button',
			'block' => 'a',
			'classes' => 'btn btn-danger btn-wire',
			'wrapper' => true,
		),
		array(
			'title' => 'Wire Button - LG',
			'block' => 'a',
			'classes' => 'btn btn-danger btn-wire btn-lg',
			'wrapper' => true,
		),
		array(
			'title' => 'Wire Button - XL',
			'block' => 'a',
			'classes' => 'btn btn-danger btn-wire btn-xl',
			'wrapper' => true,
		),
		array(
			'title' => 'Wire Button Small',
			'block' => 'a',
			'classes' => 'btn btn-danger btn-wire btn-sm',
			'wrapper' => true,
		),
		array(
			'title' => 'Line Height .5',
			'selector' => 'p,div,h1,h2,h3,h4,h5,h6',
			'classes' => 'line-height-xxs'
			),
		array(
			'title' => 'Line Height .8',
			'selector' => 'p,div,h1,h2,h3,h4,h5,h6',
			'classes' => 'line-height-xs'
			),
		array(
			'title' => 'Line Height 1',
			'selector' => 'p,div,h1,h2,h3,h4,h5,h6',
			'classes' => 'line-height-s'
			),
		array(
			'title' => 'Line Height 1.2',
			'selector' => 'p,div,h1,h2,h3,h4,h5,h6',
			'classes' => 'line-height-m'
			),
		array(
			'title' => 'Line Height 1.4',
			'selector' => 'p,div,h1,h2,h3,h4,h5,h6',
			'classes' => 'line-height-l'
			),
		array(
			'title' => 'Line Height 1.6',
			'selector' => 'p,div,h1,h2,h3,h4,h5,h6',
			'classes' => 'line-height-xl'
			),
		array(
			'title' => 'Line Height 1.8',
			'selector' => 'p,div,h1,h2,h3,h4,h5,h6',
			'classes' => 'line-height-xxl'
			),
		array(
			'title' => 'Add Padding Top Small',
			'selector' => 'p,div,span,img,figure,h1,h2,h3,h4,h5,h6',
			'classes' => 'padding-sm-top'
			),
		array(
			'title' => 'Add Padding Top Medium',
			'selector' => 'p,div,span,img,figure,h1,h2,h3,h4,h5,h6',
			'classes' => 'padding-md-top'
			),
		array(
			'title' => 'Add Padding Top Large',
			'selector' => 'p,div,span,img,figure,h1,h2,h3,h4,h5,h6',
			'classes' => 'padding-lg-top'
			),
		array(
			'title' => 'Add Padding Top XL',
			'selector' => 'p,div,span,img,figure,h1,h2,h3,h4,h5,h6',
			'classes' => 'padding-xl-top'
			),
		

	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

//// Get Styles to appear in dropdown 
add_filter( 'tiny_mce_before_init', 'fb_mce_before_init' );
function fb_mce_before_init( $settings ) {

    // customize as desired

    // unset the preview styles
    unset($settings['preview_styles']);

    return $settings;
}