<?php
/**
 * Load Typekit Fonts function.
 *
 * @access public
 * @since 3.2.2
 */

function ltc_load_fonts() {
	    wp_enqueue_script('ltc-typekit', '//use.typekit.net/hmk3fil.js'); //load in header
	}
add_action('wp_enqueue_scripts', 'ltc_load_fonts');


// ref: http://wptheming.com/2013/02/typekit-code-snippet/
function theme_typekit_inline() {
  if ( wp_script_is( 'ltc-typekit', 'done' ) ) { ?>
  	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<?php }
}
add_action( 'wp_head', 'theme_typekit_inline' );

// http://wordpress.stackexchange.com/questions/59628/editor-styles-and-typekit/61917#61917
// Display in admin 
add_filter("mce_external_plugins", "tomjn_mce_external_plugins");
function tomjn_mce_external_plugins($plugin_array){
    $plugin_array['typekit']  =  get_template_directory_uri().'/assets/js/min/typekit_plugin.min.js';
    return $plugin_array;
}