<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

////////////////////////////////////////////////////////////////// White Label Admin Pages



////////////////////////////////////////////////////////////////// ADD FUNCTION TO CHECK IF LOGIN PAGE
/////  http://stackoverflow.com/questions/5266945/wordpress-how-detect-if-current-page-is-the-login-page
function is_login_page() {
    return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
}

// Set "Remember Me" to checked
// ref: https://premium.wpmudev.org/blog/customize-login-page/

function login_checked_remember_me() {
add_filter( 'login_footer', 'rememberme_checked' );
}
add_action( 'init', 'login_checked_remember_me' );

function rememberme_checked() {
echo "<script>document.getElementById('rememberme').checked = true;</script>";
}

///////////////////////////////////////////////////////////////  Login page styling

//* Replace WordPress login logo with your own
    // http://codex.wordpress.org/Customizing_the_Login_Form

function ltc_custom_login_styles() {
echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/assets/css/plugins/wp_login.css" />';
}
add_action('login_head', 'ltc_custom_login_styles');

//* Change the URL of the WordPress login logo

function ltc_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'ltc_login_logo_url' );

function ltc_login_logo_url_title() {
    return 'Vie13 Kustom Apparel';
}
add_filter( 'login_headertitle', 'ltc_login_logo_url_title' );

// Redirect to homepage unless you are an admin

function admin_login_redirect( $redirect_to, $request, $user )
{
    global $user;
    if( isset( $user->roles ) && is_array( $user->roles ) ) {
    if( in_array( "administrator", $user->roles ) ) {
        return $redirect_to;
    } else {
        return home_url();
    }
    } else {
        return $redirect_to;
    }
}
add_filter("login_redirect", "admin_login_redirect", 10, 3);

?>