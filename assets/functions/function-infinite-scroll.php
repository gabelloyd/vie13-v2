<?php

/**
 * Customer Infinite Scroll logic based on Bill Erikson tutorial
 * @since 1.0
 * @author Gabe Lloyd
 * 
 *
 */

/**
 * Javascript for Load More
 * @since 1.0
 * @author Gabe Lloyd
 * 
 *
 */
function be_load_more_js() {
	global $wp_query;
	
	// added nonce 
	// https://gist.github.com/AlphaBlossom/845721ab5c556933de0e6092296944b5
	// Don't show load more on single pages, posts, etc except for blog home page
	// Add in a check if is woocommmerce page or shop page
	if( !is_home() && is_singular() || is_woocommerce() || is_shop() )
	    return;

	$args = array(
		'nonce' => wp_create_nonce( 'be-load-more-nonce' ),
		'url'   => admin_url( 'admin-ajax.php' ),
		'query' => $wp_query->query,
		'maxpage' => $wp_query->max_num_pages,
	);
			
	wp_enqueue_script( 'be-load-more', get_stylesheet_directory_uri() . '/assets/js/load-more.js', array( 'jquery' ), '1.0', true );
	wp_localize_script( 'be-load-more', 'beloadmore', $args );
	
}
add_action( 'wp_enqueue_scripts', 'be_load_more_js', 9 );

/**
 * AJAX Load More 
 * @link http://www.billerickson.net/infinite-scroll-in-wordpress
 */
function be_ajax_load_more() {
	$args = isset( $_POST['query'] ) ? array_map( 'esc_attr', $_POST['query'] ) : array();
	$args['post_type'] = isset( $args['post_type'] ) ? esc_attr( $args['post_type'] ) : 'post' ;
	$args['paged'] = esc_attr( $_POST['page'] );
	$args['post_status'] = 'publish';

	ob_start();
	$loop = new WP_Query( $args );
	if( $loop->have_posts() ): while( $loop->have_posts() ): $loop->the_post();
		
		get_template_part( 'template-parts/post/content', get_post_format() );
			
	endwhile; endif; wp_reset_postdata();
	$data = ob_get_clean();
	wp_send_json_success( $data );
	wp_die();
}
add_action( 'wp_ajax_be_ajax_load_more', 'be_ajax_load_more' );
add_action( 'wp_ajax_nopriv_be_ajax_load_more', 'be_ajax_load_more' );