<?php
/**
 * Load Fontawesome function.
 *
 * @access public
 * @since 3.4
 */

function load_fontawesome() {
	    wp_enqueue_script('ltc_fontawesome', '//use.fontawesome.com/21080b64fc.js'); //load in header
	}
add_action('wp_enqueue_scripts', 'load_fontawesome');