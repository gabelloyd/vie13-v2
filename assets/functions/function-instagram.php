<?php /**
 * get_instagram_photos function.
 *
 * @access public
 * @param int $limt (default: 8)
 * @return void
 */
function get_instagram_photos($limt=8, $tag="") {
	$html=null;

	$args=array(
		'posts_per_page' 	=> $limt, // number of posts
		'tag'				=> $tag, // get tags
		'category_name'		=> "latest" // only get the "latest" category
	);
	$posts=get_posts($args);

	if (!count($posts))
		return false;

	$html.='<div class="grid">';
		$html.='<div class="grid-sizer"></div>';
			$html.='<div class="gutter-sizer"></div>';

	foreach ($posts as $post) :

		$link				=	get_permalink( $post->ID );
		$date 				=	get_the_time(get_option('date_format'), $post->ID);
		$caption = get_post_meta($post->ID, 'igp_caption', true);


		// These need to be in this order to keep the different counts straight since I used either $instagram_likes or $instagram_comments throughout.
        // LIKES 
        $instagram_likes 	=	get_post_meta($post->ID, 'ig_likes', true);
        // here we add a , for all numbers below 9,999
        if (isset($instagram_likes) && $instagram_likes <= 9999) {
            $instagram_likes = number_format($instagram_likes);
        }
        // here we convert the number for the like count like 1,200,000 to 1.2m if the number goes into the millions
        if (isset($instagram_likes) && $instagram_likes >= 1000000) {
            $instagram_likes = round(($instagram_likes / 1000000), 1) . 'm';
        }
        // here we convert the number for the like count like 10,500 to 10.5k if the number goes in the 10 thousands
        if (isset($instagram_likes) && $instagram_likes >= 10000) {
            $instagram_likes = round(($instagram_likes / 1000), 1) . 'k';
        }
        // COMMENTS 
        $instagram_comments = get_comments_number($post->ID); // get_comments_number returns only a numeric value
        // here we add a , for all numbers below 9,999
        if (isset($instagram_comments) && $instagram_comments <= 9999) {
            $instagram_comments = number_format($instagram_comments);
        }
        // here we convert the number for the comment count like 1,200,000 to 1.2m if the number goes into the millions
        if (isset($instagram_comments) && $instagram_comments >= 1000000) {
            $instagram_comments = round(($instagram_comments / 1000000), 1) . 'm';
        }
        // here we convert the number  for the comment count like 10,500 to 10.5k if the number goes in the 10 thousands
        if (isset($instagram_comments) && $instagram_comments >= 10000) {
            $instagram_comments = round(($instagram_comments / 1000), 1) . 'k';
        }


		$html.='<div class="grid-item">';
			$html.='<div class="front">';
				$html.=get_the_post_thumbnail($post->ID, 'medium-square', array( // medium-square is 300x300 hard crop
								'class' => 'instagram-thumbs',
								'title' => get_the_title($post->ID),
								));
					$html.='<a href="'.$link.'" title="'.$caption.'">';
						$html.='<div class="back">';
							$html.='<div class="instagram-comments-likes-wrapper" layout="rows center-center">';
								$html.='<div class="instagram-likes-wrapper">';
									$html.='<span class="glyphicon glyphicon-heart"></span><div class="instagram-likes">'.$instagram_likes.'</div>';
								$html.='</div><!-- instagram-likes-wrapper-->';
								$html.='<div class="instagram-comments-wrapper">';
									$html.='<span class="glyphicon glyphicon-comment"></span><div class="instagram-comments">'.$instagram_comments.'</div>';
								$html.='</div><!-- instagram-comments-wrapper-->';
								$html.='<div class="instagram-date">'.$date.'</div>';
							$html.='</div><!-- instagram-comments-likes-wrapper-->';
							
						$html.='</div><!-- .back -->';
					$html.='</a>';
			$html.='</div><!--  .front-->';
			
		$html.='</div>';


	endforeach;


		$html.='</div>';


	return $html;
}


///////////////////////////////////////////////////////// Instagrate Pro Shortcode

/**
 * Create a shortcode to drop onto pages to display the IG feed.
 * shortcode is [instagram]
 * shortcode extension [instagram limit=8 tag="foo"]
 * Tags can be linked or excluded based on logic from joining php terms.
 * Use a comma to setup "ALSO" [instagram tags="foo, bar"] shows both foo and bar 
 * Use a plus to show posts that have only both [instagram tags="foo + bar"] shows posts that only have foo and bar, but not posts that only include one or the other. 
 * @since 1.0
 * @author Gabe Lloyd
 */
function igp_shortcode($atts) {

	$atts=shortcode_atts(array(
		'limit' 			=> 8, // number of posts
		'tag'				=> '', // tag filter
	),$atts);

	ob_start();
	return get_instagram_photos($atts['limit'], $atts['tag']);
	return ob_get_clean();
}
add_shortcode( 'instagram', 'igp_shortcode' );

///////////////////////////////////////////////////////// Custom Footer for posts 
if ( ! function_exists( 'vie13_igp_entry_footer' ) ) :
/**
 * Prints HTML with meta information to display the tags (hashtags on IG) and display comments from IG here on WP. 
 * This is different from the regular footer because we do not show category.
 * @since 1.0
 * @author Gabe Lloyd
 */
function vie13_igp_entry_footer() { 

	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '#', esc_html__( ' #', 'vie13' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( '%1$s', 'vie13' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}
	
	if (is_single()) : // on single posts we show the regular link
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'vie13' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	else : // on archive pages we show the edit link next to each post 
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'vie13' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="post-edit-link">',
			'</span>'
		);
	endif; // end single post check 
		
	
} // end function 
endif; // end function exists check


function vie13_igp_likes_comments_count(){
	// Show the likes and comments count first 
	global $post;
	// Show likes and comments here 
	// LIKES 
    $instagram_likes 	=	get_post_meta( $post->ID, 'ig_likes', true);
    // here we add a , for all numbers below 9,999
    if (isset($instagram_likes) && $instagram_likes <= 9999) {
        $instagram_likes = number_format($instagram_likes);
    }
    // here we convert the number for the like count like 1,200,000 to 1.2m if the number goes into the millions
    if (isset($instagram_likes) && $instagram_likes >= 1000000) {
        $instagram_likes = round(($instagram_likes / 1000000), 1) . 'm';
    }
    // here we convert the number for the like count like 10,500 to 10.5k if the number goes in the 10 thousands
    if (isset($instagram_likes) && $instagram_likes >= 10000) {
        $instagram_likes = round(($instagram_likes / 1000), 1) . 'k';
    }
    // COMMENTS 
    $instagram_comments = get_comments_number($post->ID); // get_comments_number returns only a numeric value
    // here we add a , for all numbers below 9,999
    if (isset($instagram_comments) && $instagram_comments <= 9999) {
        $instagram_comments = number_format($instagram_comments);
    }
    // here we convert the number for the comment count like 1,200,000 to 1.2m if the number goes into the millions
    if (isset($instagram_comments) && $instagram_comments >= 1000000) {
        $instagram_comments = round(($instagram_comments / 1000000), 1) . 'm';
    }
    // here we convert the number  for the comment count like 10,500 to 10.5k if the number goes in the 10 thousands
    if (isset($instagram_comments) && $instagram_comments >= 10000) {
        $instagram_comments = round(($instagram_comments / 1000), 1) . 'k';
    }

    echo '<div class="instagram-comments-likes-wrapper">';
		echo '<div class="instagram-likes-wrapper">';
			echo '<span class="glyphicon glyphicon-heart"></span><div class="instagram-likes">'.$instagram_likes.'</div>';
		echo '</div><!-- instagram-likes-wrapper-->';
		echo '<div class="instagram-comments-wrapper">';
			echo '<span class="glyphicon glyphicon-comment"></span><div class="instagram-comments">'.$instagram_comments.'</div>';
		echo '</div><!-- instagram-comments-wrapper-->';
	echo '</div><!-- instagram-comments-likes-wrapper-->';
}


////////////// Filter comment reply link for image post
// define the comment_reply_link callback 
function filter_comment_reply_link( $args_before_link_args_after, $args, $comment, $post ) { 
	// make filter magic happen here…
	$format = get_post_format();
	if($format == 'image' || 'video'){
		return null;
	} else {
    	return $args_before_link_args_after; 
	}
}; 
         
// add the filter 
add_filter( 'comment_reply_link', 'filter_comment_reply_link', 10, 4 );


/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 22;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( '...', 'vie13' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

//// FIX ACF BUG THAT HIDES CUSTOM FIELDS 
add_filter('acf/settings/remove_wp_meta_box', '__return_false');
// https://support.advancedcustomfields.com/forums/topic/acf-pro-5-6-0-unable-to-display-regular-custom-fields-metabox-on-cpts/