<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////  REMOVE WPAUTOP ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

//remove_filter( 'the_content', 'wpautop' ); // remove p tags from the_content

add_filter('the_content', 'remove_empty_p', 20, 1);
function remove_empty_p($content){
    $content = force_balance_tags($content);
    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}

// remove p tags from images
// https://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/
function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');


// Wrap Tablepress in a class so we can assign Bootstrap classes
// https://wordpress.org/support/topic/adding-class-to-table-name-and-description?replies=4
// add_filter( 'tablepress_table_output', 'tp_add_div_wrapper', 10, 3 );
// function tp_add_div_wrapper( $output, $table, $render_options ) {
//   $output = "<div class='tablepress-wrapper {$render_options['extra_css_classes']}'>\n{$output}\n</div>";
//   return $output;
// }

?>