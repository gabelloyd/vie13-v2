<?php 

// FRONT PAGE

require get_template_directory() .'/assets/functions/acf_metaboxes/functions-acf-meta-front-page.php';

// HEADER OPTIONS

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_56b6a45754b1f',
	'title' => 'Header Options',
	'fields' => array (
		array (
			'key' => 'field_56f029e3bf1b6',
			'label' => 'Mobile Image',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_56f02a2d08e20',
			'label' => 'Mobile Header Image',
			'name' => 'mobile_header_image',
			'type' => 'image',
			'instructions' => 'Upload the image to be used on mobile',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => 50,
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_56f02ac5475f0',
			'label' => 'Mobile Header Image Dark',
			'name' => 'mobile_header_image_dark',
			'type' => 'image',
			'instructions' => 'Upload the image to be placed on mobile on the homepage, which has a dark background.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => 50,
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 1,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


// FOOTER OPTIONS

acf_add_local_field_group(array (
	'key' => 'group_562119a11e1a6',
	'title' => 'Footer Options',
	'fields' => array (
		array (
			'key' => 'field_562119a6d6e70',
			'label' => 'Copyright Text',
			'name' => 'copyright_by_text',
			'type' => 'text',
			'instructions' => 'Enter the name of who should appear next to the Copyright logo in the footer. This will default to the site name if nothing is entered.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => 142,
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options',
			),
		),
	),
	'menu_order' => 2,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

?>