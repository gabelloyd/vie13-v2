<?php
/**
 * The template for displaying archive pages.
 * Wrap container in class singleColumn to align with theme styles 
 * Add own comment template and styling 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vie13
 */

get_header(); ?>

	<div id="primary" class="content-area singleColumn">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header>
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/post/content', get_post_format() );

			endwhile;

			the_posts_pagination( array(
				'prev_text' => '<span class="nav-post-indicator glyphicon glyphicon-chevron-left"><span class="screen-reader-text">' . __( 'Previous page', 'vie13' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'vie13' ) . '</span><span class="nav-post-indicator glyphicon glyphicon-chevron-right"></span>',
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'vie13' ) . ' </span>',
			) );

		else :

			get_template_part( 'template-parts/post/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
