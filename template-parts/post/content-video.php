<?php
/**
 * Template part for displaying single posts formatted for videos.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vie13
 * @since 1.0
 * @author Gabe Lloyd
 */

?>

<article id="post-<?php the_ID(); ?>"  
	<?php if (is_single()) { // Change the container size on archive or single post 
			post_class('container'); 
			echo 'layout="sm-column row center-center">';
		} else {
			post_class('singleColumn archive');
		} ?>

		<header class="entry-header">
		<?php // POST IMAGE //	
		if (has_post_thumbnail()) {
			
			echo '<div class="entry-thumbnail ">';
			
				// CHECK FOR STICKY POST 
				if (is_sticky()) {
					echo '<i class="fa fa-thumb-tack sticky-post" aria-hidden="true"></i>';
				}
				if (is_single()) {
					// do nothing
				} else {
					
					echo '<a href="'.get_permalink().'" title="' . __('Click to watch ', 'vie13') . get_the_title() . '" rel="bookmark">';
					echo '<i class="fa fa-3x fa-video-camera fa-border video-overlay" aria-hidden="true"></i>';
					echo the_post_thumbnail(array(700,700)); // show a sqaure image 
					echo '</a>';
				}
			
			echo '</div>';
			} else { // else, if there is not a Featured Image 
				
				echo '<div class="no-thumbnail"></div>';
				
			} // ENDS THE POST IMAGE ?>
		</header><!-- .entry-header -->
	
		<?php if (is_single()) { ?>
			<div class="image-content-container"> <?php //wrap the content and footer in this container ?>
				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- entry-content -->
		<?php } ?>

		<?php // Change column or row based on single or archive 
		if (is_single()) {
			$grid = 'column';
		} else {
			$grid = 'row';
		} ?>
		
		<footer class="entry-footer" layout="<?php echo $grid;?> center-stretch">
			<?php
			if (is_single()):

			
				vie13_posted_on();
				vie13_entry_footer();
				// Set the text on the post to be the IG Caption 
				$caption = get_post_meta($post->ID, 'igp_caption', true);

				if($caption):
					echo '<div class="clearfix">';
					echo $caption;
					echo '</div>';
				else:
					echo '<div class="clearfix title">';
					the_title();
					echo '</div>';
				endif;
		

				// Setup Comments and IG.
				// This is all under the title, meta, caption 
				
				// Button to original IG post 
				// if there is a link to instagram, we'll show a button to IG post 

				$link =	get_post_meta($post->ID, 'igp_url', true);

				// Comments
				
				
				
				// If there are any posts here on WP, let's show them. 
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					echo '<div class="singleColumn">';
						comments_template();
					echo '</div>';
				endif;

				// This is under the IG button link.
				// Displays if there are comments on this post in WP 
				
					if($link):
						echo '<div class="padding-md-bottom">';
						echo '<a class="btn btn-block btn-lg btn-wire btn-danger" href="'.$link.'" role="button">';
						echo 'Like or Comment on Instagram';
						echo '</a>';
						echo '</div>';
					endif; // end the big call to action for commenting on IG

			else : // what we show on the archive page 
				if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta" self="size-x3">
						<?php vie13_posted_on(); ?>
						<?php vie13_entry_footer(); ?>
					</div><!-- .entry-meta -->
				<?php endif; // end the post type check  ?>
				<div class="btn-read-more">
					<a href="<?php echo esc_url( get_permalink($post->ID) ); ?>" class="btn btn-wire btn-default" role="button">Watch Video</a>
				</div>
			<?php endif; // end single check ?>

		</footer> <!-- entry-footer -->
	<?php if (is_single()) { ?>
		</div> <!-- image-content-container -->
	<?php } ?>
</article>