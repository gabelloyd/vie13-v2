<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Vie13_Theme_2
 */

?>

<?php $format = get_post_format(); // store the post format here because we keep using this through out the page ?>

	<article id="post-<?php the_ID(); ?>" <?php if (is_single()) { // Change the container size on archive or single post 
			post_class('container'); 
		} else {
			post_class('singleColumn');
		} ?> >

	<?php // POST IMAGE //	
	if (has_post_thumbnail()) {
		if (is_single()) {
			// Setup the featured image for the post above the article 
			// ref: http://wordpress.stackexchange.com/questions/4745/getting-thumbnail-path-rather-than-image-tag?rq=1
			// ref: https://developer.wordpress.org/reference/functions/the_post_thumbnail/
			// Set the post thumbnail image attributes when we use it as a background image 

			if(is_mobile()){
				$imageArray = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' );
			} elseif (is_tablet()) {
				$imageArray = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
			} else {
				$imageArray = wp_get_attachment_image_src( get_post_thumbnail_id(), 'max-fullsize' );
			}

			$imageAlt = get_post_meta($post->ID, '_wp_attachment_image_alt', true);
			$imageURI = $imageArray[0]; // 0 is the URI
			$imageWidth = $imageArray[1]; // 1 is the width
			$imageHeight = $imageArray[2]; // 2 is the height

			echo '<div class="article-bg-header-wrap">';
			echo '<div class="fill" style="background-image:url('.$imageURI.')" alt="'.$imageAlt.'" width="'.$imageWidth.'" height="'.$imageHeight.';"></div>';
			echo '</div>'; ?>
			
			<?php } else { // else, not single  ?>
			
				<?php echo '<div class="entry-thumbnail">';
					echo '<a href="'.get_permalink().'" title="' . __('Click to read ', 'vie13') . get_the_title() . '" rel="bookmark">';
						// CHECK FOR STICKY POST 
						if (is_sticky()) {
							echo '<i class="fa fa-thumb-tack sticky-post"></i>';
						} else {
							echo the_post_thumbnail('archive-thumb'); // show a 16:9 aspect ratio image 
						}
					
					echo '</a>';
				echo '</div>';
				} // end Featured Thumbnail check for single post, single format-image, archive page check 
			} else { // else, if there is not a Featured Image 
				$no_thumbnail = 'no-thumbnail'; // set a variable here that we're going to use in the <header> 
			} // ENDS THE POST IMAGE ?>
			
			<header class="entry-header <?php if (!has_post_thumbnail()) { echo $no_thumbnail; }?>">
			
			<?php // POST TITLE //			
			if ( is_single()) { // we don't want to show the title on format-image posts 
				the_title( '<h1 class="entry-title singleColumn">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}

			?>
			</header><!-- .entry-header -->

	<div class="entry-content singleColumn">
		<?php
		if ( is_single() ) : ?>
			<div class="entry-meta">
				<?php vie13_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'vie13' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		else :

		/* translators: %s: Name of current post */
			the_excerpt( sprintf(
				__( 'Continue reading %s', 'vie13' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );
		endif;
		wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vie13' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer" layout="row center-stretch">
		<?php if ( is_single() ) :
		
		else :
			if ( 'post' === get_post_type() ) : ?>
				<div class="entry-meta" self="size-x3">
					<?php vie13_posted_on(); ?>
					<?php vie13_entry_footer(); ?>
				</div><!-- .entry-meta -->
				<div self="right" class="btn-read-more">
					<a href="<?php echo esc_url( get_permalink($post->ID) ); ?>" class="btn btn-wire btn-default" role="button">Read Post</a>
				</div>
				<?php else: ?>
				<div self="right" class="btn-read-more">
					<a href="<?php echo esc_url( get_permalink($post->ID) ); ?>" class="btn btn-wire btn-default" role="button">Read More&hellip;</a>
				</div>
			<?php endif; ?>

	<?php endif; ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
