<?php
/**
 * Template part for displaying single posts as aside formats. We remove the feature image here explicitly.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package vie13
 * @since 1.0
 * @author Gabe Lloyd
 */

?>


<article id="post-<?php the_ID(); ?>" <?php post_class('singleColumn'); ?> >

	<header class="entry-header">
		<?php if (is_sticky()) {
				echo '<i class="fa fa-thumb-tack sticky-post"></i>';
			} ?>
		<?php
		// POST TITLE //			
			if ( is_single() ) { // we don't want to show the title on format-image posts 
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
		?>

		<div class="entry-meta text-center">
			
		</div><!-- .entry-meta -->
		
		
	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php
		if ( is_single() ) : ?>
			
			<?php the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'vie13' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		else :

		/* translators: %s: Name of current post */
			the_excerpt( sprintf(
				__( 'Continue reading %s', 'vie13' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );
		endif;
		wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vie13' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- entry-content -->
		<footer class="entry-footer">
			<?php if ( is_single() ) : ?>
				<div class="entry-meta" self="size-x3">
					<?php vie13_posted_on(); ?>

				</div><!-- .entry-meta -->
		
				<?php else :
					if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta" self="size-x3">
							<?php vie13_posted_on(); ?>

						</div><!-- .entry-meta -->
					<?php endif; ?>
				<div self="right" class="btn-read-more">
					<a href="<?php echo esc_url( get_permalink($post->ID) ); ?>" class="btn btn-wire btn-default" role="button">Read Post</a>
				</div>
			<?php endif; ?>
		</footer><!-- .entry-footer -->
	
	</div><!-- .entry-content -->
</article><!-- #post-## -->