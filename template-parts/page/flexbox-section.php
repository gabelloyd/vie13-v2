<?php 
$display_in_menu = get_sub_field('display_section_in_menu');
$menu_display_name = title_to_id(get_sub_field('menu_display_name_v2'));

// Background image Controls for the outer section 
$display_background_image 	= get_sub_field('display_background_image');
$sectionBGColor 			= get_sub_field('bg_color');
$sectionBGImage 			= get_sub_field('bg_image');
$sectionBGRepeat 			= get_sub_field('bg_repeat');
$sectionBGPosition 			= get_sub_field('bg_position');
$sectionBGAttachment 		= get_sub_field('bg_attach');
$sectionBGSize 				= get_sub_field('bg_size');
// end 

$section_padding_array = get_sub_field_object('section_padding');
	$value = $section_padding_array['value'];
	if ($value) {
		$section_padding_array_output = $value;
	} else {
		$section_padding_array_output = null;
	}

$section_margin_array = get_sub_field_object('section_margin');
	$value = $section_margin_array['value'];
	if ($value) {
		$section_margin_array_output = $value;
	} else {
		$section_margin_array_output = null;
	}

// Blocks Area

	$get_inner_wrapper_width = get_sub_field('inner_wrapper_width');
	if ($get_inner_wrapper_width = 'full-width') {
		$inner_wrapper_width = null;
	} else {
		$inner_wrapper_width = get_sub_field('inner_wrapper_width');
	}


// Section Height
$height_amount = get_sub_field( 'height_value' );
$height_unit = get_sub_field( 'height_unit' );

// Section Flexbox settings
$flex_row_column = get_sub_field('flex_row_column');
$vert_align_content_input = get_sub_field('vert_align_content_input');
$hor_align_content_input = get_sub_field('hor_align_content_input');
$section_text_color = get_sub_field('section_text_color');

// Section Optional Classes 
$optional_classes_1 = get_sub_field('optional_classes_1');
?>


<section <?php if ($display_in_menu === TRUE) {echo 'id="'. $menu_display_name .'"';}?> class="page-section full-width flexbox-section os-animation <?php if($section_text_color) { echo $section_text_color.' '; } ?><?php echo $section_padding_array_output.' ';?><?php echo $section_margin_array_output;?>" data-os-animation="fadeIn" data-os-animation-delay="0s" 
	<?php
	 if ($display_background_image === TRUE) { ?>
	  	style="background:<?php if($sectionBGColor){echo $sectionBGColor;} else {echo'transparent';} ?> url(<?php echo $sectionBGImage; ?>) <?php echo $sectionBGRepeat ?> <?php echo $sectionBGPosition ?>; -webkit-background-size:<?php echo $sectionBGSize;?>; -moz-background-size:<?php echo $sectionBGSize;?>; -o-background-size:<?php echo $sectionBGSize;?>; background-size:<?php echo $sectionBGSize;?>; background-attachment: <?php echo $sectionBGAttachment ?>"
		<?php
	  } else { ?>
	  	style="background-color:<?php if($sectionBGColor){echo $sectionBGColor;} else {echo'transparent';} ?>; background-size:<?php echo $sectionBGSize;?>;"
	<?php  } ?>
	> <?php // end section class declarations ?>
					
		<?php // Flexbox divs inside the container to create content
		if ( have_rows( 'flex_containers' ) ) : ?>
			<div class="<?php echo $inner_wrapper_width; ?>">
				<div <?php if ($optional_classes_1) {echo 'class="'.$optional_classes_1.'"';}?><?php if($height_amount){ echo 'style="height:'.$height_amount.$height_unit.'"';}?> layout="<?php echo $flex_row_column;?> <?php echo $vert_align_content_input;?>-<?php echo $hor_align_content_input;?>">
				<?php while ( have_rows( 'flex_containers' ) ) : the_row();
					
					// Background Image controls for the inner content divs 
					$inner_content_background_display_background_image 	= get_sub_field( 'inner_content_background_display_background_image');
					$inner_content_background_bg_color				= get_sub_field( 'inner_content_background_bg_color');
					$inner_content_background_bg_image 				= get_sub_field( 'inner_content_background_bg_image');
					$inner_content_background_bg_repeat				= get_sub_field( 'inner_content_background_bg_repeat');
					$inner_content_background_bg_position			= get_sub_field( 'inner_content_background_bg_position');
					$inner_content_background_bg_attach				= get_sub_field( 'inner_content_background_bg_attach');
					$inner_content_background_bg_size				= get_sub_field( 'inner_content_background_bg_size');
					// end 

					// Padding and Margin for Blocks
					$inner_content_padding						= get_sub_field( 'content_padding_tb');
					$inner_content_margin						= get_sub_field( 'block_margin');
					$inner_content_width							= get_sub_field( 'content_input_width');

					// Block Self styling
					$self_styling									= get_sub_field( 'self_styling' );
					$optional_classes_2								= get_sub_field('optional_classes_2');
					?>

					<div <?php if($self_styling){ echo 'self="'.$self_styling.'"';}?> class="<?php if ($optional_classes_2) {echo $optional_classes_2.' ';}?><?php if($inner_content_padding){echo $inner_content_padding.' '; } if($inner_content_margin){echo $inner_content_margin.' '; } if($inner_content_width){echo $inner_content_width; }?>"
					<?php
					 if ($inner_content_background_display_background_image === TRUE) { ?>
					  	style="background:<?php if($inner_content_background_bg_color){echo $inner_content_background_bg_color;} else {echo'transparent';} ?> url(<?php echo $inner_content_background_bg_image; ?>) <?php echo $inner_content_background_bg_repeat.' ' ?><?php echo $inner_content_background_bg_position.' ' ?><?php echo $inner_content_background_bg_attach ?>; -webkit-background-size:<?php echo $inner_content_background_bg_size;?>; -moz-background-size:<?php echo $inner_content_background_bg_size; ?>; -o-background-size:<?php echo $inner_content_background_bg_size;?>; background-size:<?php echo $inner_content_background_bg_size;?>;"
						<?php
					  } else { ?>
					  	style="background-color:<?php if($inner_content_background_bg_color){echo $inner_content_background_bg_color;} else {echo'transparent';} ?>; background-size:<?php echo $inner_content_background_bg_size;?>;"
					<?php  } ?>

					>
					<?php the_sub_field( 'column_content_input' ); ?>
					</div>
				<?php endwhile; ?>
				</div> <!-- end the layout -->
			</div> <!-- end the inner container wrapper width -->
		<?php else : ?>
			<?php // no rows found ?>
		<?php endif; ?>

</section><!-- end the section --> 
