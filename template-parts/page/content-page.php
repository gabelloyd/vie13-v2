<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Vie13_Theme_2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> layout="column stretch-stretch" >
	
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title sr-only">', '</h1>' ); ?>
		<?php if ( get_field( 'display_media_area' ) == 1 ): //check if page has this section active with "true" in admin area.
			get_template_part('template-parts/header/content', 'display_multi_media_area_section');	
		endif; //end check for display_full_screen_area check?>
	</header><!-- .entry-header -->
	
	<div class="entry-content">
		<?php if($post->post_content=="") : //if the_content is blank, don't show it.
				// do nothing so the divs are not rendered in the DOM.
			else :
				// ACF controls for content styling
				$wrapper_width = get_field('the_content_wrapper_size'); ?>
					<div class="<?php echo $wrapper_width;?>">
						
						<?php the_content(); ?>
						
					</div>

		<?php endif; // end the_content() check ?>

		<?php // start the custom layout options
		// check if the flexible content field has rows of data
		if( have_rows('flexible_container') ):

		 	// loop through the rows of data
		    while ( have_rows('flexible_container') ) : the_row(); ?>

			<?php if ( get_row_layout() == 'content_editor' ) : ?>

			 		<?php //grab variables
			 		$wrapper_width = get_sub_field('the_content_wrapper_size');
		        	$section_padding = get_sub_field('section_padding');
		        	$section_margin = get_sub_field('section_margin');
		        	$content_input = get_sub_field('content_input');       
				    $bg_color = get_sub_field('bg_color');?>

		        	<div class="<?php echo $wrapper_width;?>" <?php if ($bg_color): ?>
		        		style="background-color:<?php echo $bg_color;?>;";
		        	<?php endif ?>>
			        	<div class="<?php echo $section_padding;?> <?php echo $section_margin;?>">	
	        				<?php echo $content_input;?>
			        	</div>	
		        	</div> 
				   
			<?php elseif ( get_row_layout() == 'flexbox_repeater_layout' ) : ?>

				<?php if ( have_rows( 'page_content_sections' ) ):
					while ( have_rows( 'page_content_sections' ) ) : the_row();
						get_template_part('template-parts/page/flexbox', 'section');
					endwhile; 
				endif; //end check for page_content_sections check?>
			<?php elseif ( get_row_layout() == 'multi_media_area' ) : ?>
				<?php if ( get_sub_field( 'display_multi_media_area_display_full_screen_area' ) == 1 ): //check if page has this section active with "true" in admin area.
					get_template_part('template-parts/header/content', 'display_multi_media_area_section');
				endif; //end check for display_full_screen_area check?>
			<?php endif; ?>
			<?php endwhile; ?>
		
		<?php else: ?>
			<?php // no layouts found ?>
		<?php endif;

		
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'vie13' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'vie13' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
