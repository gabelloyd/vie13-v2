<nav id="footer-site-navigation" class="footer-nav" role="navigation">

	<div layout="rows top-spread">
		<?php dynamic_sidebar( 'footer-option-1' ); ?>
		<?php dynamic_sidebar( 'footer-option-2' ); ?>
		<?php dynamic_sidebar( 'footer-option-3' ); ?>
		<?php dynamic_sidebar( 'footer-option-4' ); ?>
				
	</div>
</nav><!-- #site-navigation -->