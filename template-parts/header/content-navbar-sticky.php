<div class="fixedDiv">
	<nav id="site-navigation" class="navbar" role="navigation">
		<div class="container">
			<div layout="row center-left">
				<button id="nav-toggle" class="navbar-toggle" type="button" data-toggle="collapse" href="#fixednav" aria-expanded="false" aria-controls="fixednav" aria-label="Menu" >
					<div class="hamburger hamburger--slider">
						<span class="hamburger-box">
							<span class="hidden-xs">Menu</span>
							<span class="hamburger-inner"></span>
						</span>
					</div>
				</button>
				<div class="site-branding-secondary">
					
					<?php if ( function_exists( 'the_custom_logo' ) ) {
						the_custom_logo();
					} ?>
					
				</div><!-- .site-branding -->

				<div class="collapse navbar-collapse" id="fixednav">
							
						<?php wp_nav_menu( array(
							'theme_location' => 'primary', 
							'container' => false,
							'menu_class' => 'nav navbar-nav',
							'items_wrap' => '<ul id="%1$s" class="nav navbar-nav">%3$s</ul>',
							'walker'        => new ltc_walker_nav_menu,

						)); ?>
					
				</div>

				
				<div self="right">
					
					<?php dynamic_sidebar( 'navbar-widgets-sticky' ); ?>

				</div>
			</div>
		</div>
	</nav><!-- #site-navigation -->


</div><!-- .fixedDiv -->