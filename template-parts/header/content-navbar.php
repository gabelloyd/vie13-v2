<?php dynamic_sidebar( 'eu-cookie-widget-area' ); ?>

<nav id="site-navigation" class="navbar" role="navigation" style="background-image: url(<?php header_image(); ?>)" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php get_bloginfo( 'name' )?>">
	<div class="container">
		<div>
			<?php dynamic_sidebar( 'navbar-widgets-1' ); ?>
			<!-- Brand and toggle get grouped for better mobile display. Header is usually just the brand and collapse -->
		    <div class="navbar-header" layout="row center-left">
			    <button id="nav-toggle" class="navbar-toggle collapsed" type="button" data-toggle="collapse" href="#primarynav" aria-expanded="false" aria-label="Primary Navigation">
					<div class="hamburger hamburger--slider">
						<span class="hamburger-box">
							<span class="sr-only">Toggle navigation</span>
							<span class="hamburger-inner"></span>
						</span>
					</div>
				</button>

			    <div class="site-branding">
					<?php get_template_part('template-parts/header/content', 'site-branding'); ?>
				</div><!-- .site-branding -->
				
				<ul class="nav navbar-nav navbar-right" role="complementary">
					<?php dynamic_sidebar( 'navbar-widgets' ); ?>
				</ul>
			</div><!-- navbar-header -->
		</div>
		<div layout="rows center-left">
			<!-- Primary Navigation -->
			<?php wp_nav_menu( array(
				'theme_location' 	=> 'primary', 
				'container_class' 	=> 'collapse navbar-collapse',
				'container_id'		=> 'primarynav',
				'menu_class' 		=> 'nav navbar-nav',
				// 'items_wrap' 		=> '<ul id="%1$s">%3$s</ul>',
				// 'walker'        	=> new ltc_walker_nav_menu,

			)); ?>		
		
			
			<?php dynamic_sidebar( 'navbar-widgets-3' ); ?>
					
		</div><!-- right side -->
	</div><!-- full-width -->
</nav><!-- #site-navigation -->