<?php

$displayMultiMediaContentSelection = get_field('media_content_selection');

switch ($displayMultiMediaContentSelection) {
	case 'fullscreen_slideshow':
	$images = get_field('gallery');

	// get specific size image
	if( !empty($images) ):
		if(!is_mobile() && !is_tablet()):
			$size = 'max-fullsize'; //1400px
		elseif(is_tablet()):
			$size = 'large';//1024px
		elseif(is_mobile()):
			$size = 'medium';
		endif;
	endif;

	// run output 
	if($images):
		echo '<div id="carousel-wrapper">';
			echo '<section id="carousel-header">';
			 	echo '<div id="carousel-feature" class="carousel slide carousel-fade" data-ride="carousel">';
				 	
				 	echo '<div class="carousel-inner" role="listbox">';
				 		if( $images ) : $i = 1;
							
				 		foreach( $images as $image ): ?>
				 		<div class="item <?php if($i == 1 ) { echo 'active'; } ?>">
						<?php	
							echo '<div class="fill" style="background-image:url(' . $image['sizes'][ $size ] . ');" alt="' . $image['alt'] . '" width="' .$image['sizes'][ $size . '-width' ]. '" height="' .$image['sizes'][ $size . '-height' ]. '" ></div>';								
							echo '<div class="sr-only slide-caption-data">' . $image['caption'] . '</div>';
						echo '</div>'; // end item
						$i++; endforeach;
						endif;
					echo '</div>'; // end .carousel-inner
				echo '</div>'; // end #carousel-feature
			echo '</section>'; // end section #carousel-header
			get_template_part('template-parts/header/content', 'display_multi_media_area_overlay'); 
		echo '</div>'// end div#carousel-wrapper
			?>
			<!-- Controls -->
			<!-- <a class="left carousel-control" href="#carousel-feature" role="button" data-slide="prev">
				<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-feature" role="button" data-slide="next">
				<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
				<span class="sr-only">Next</span>
			</a> -->
			
			<?php 
			
		endif; // ends gallery
		break;
	case 'fixed_height_gallery':
	// Fixed Height Gallery

	$images = get_field('gallery');

	// get specific size image
	if( !empty($images) ):
		if(!is_mobile() && !is_tablet()):
			$size = 'max-fullsize'; //1400px
		elseif(is_tablet()):
			$size = 'large';//1024px
		elseif(is_mobile()):
			$size = 'medium';
		endif;
	endif;

	if($images):
		echo '<div id="carousel-wrapper-fixed">';
			echo '<section id="carousel-header-fixed">';
			 	echo '<div id="carousel-feature" class="carousel slide carousel-fade" data-ride="carousel">';
				 	
				 	echo '<div class="carousel-inner" role="listbox">';
				 		if( $images ) : $i = 1;
							
				 		foreach( $images as $image ): ?>
				 		<div class="item <?php if($i == 1 ) { echo 'active'; } ?>">
						<?php	
							echo '<div class="fill" style="background-image:url(' . $image['sizes'][ $size ] . ');" alt="' . $image['alt'] . '" width="' .$image['sizes'][ $size . '-width' ]. '" height="' .$image['sizes'][ $size . '-height' ]. '" ></div>';								
							echo '<div class="sr-only slide-caption-data">' . $image['caption'] . '</div>';
						echo '</div>'; // end item
						$i++; endforeach;
						endif;
					echo '</div>'; // end .carousel-inner
				echo '</div>'; // end #carousel-feature
			echo '</section>'; // end section #carousel-header
			get_template_part('template-parts/header/content', 'display-media-overlay'); 
			
			?>
			<!-- Controls -->
			<!-- <a class="left carousel-control" href=".carousel.slide" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href=".carousel.slide" role="button" data-slide="next">
				<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a> -->
		<?php	
		echo '</div>';// end div#carousel-wrapper-fixed

		endif;
		break;
		case 'video':
		// video
			// get iframe HTML
			$iframe = get_field('video');


			// We have to get the Video ID from the oEmbed in of the ACF field. 
			function get_youtube_id_from_url($url)
				{
				    if (stristr($url,'youtu.be/'))
				        {preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID); return $final_ID[4]; }
				    else 
				        {@preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD); return $IDD[5]; }
				}


			$src = get_youtube_id_from_url($iframe);

			// use preg_match to find iframe src
			// preg_match('/src="(.+?)"/', $iframe, $matches);
			// $src = $matches[1];


			// // add extra params to iframe src
			// $params = array(
			// 'controls'    	=> 0, // Show pause/play buttons in player
			// 'hd'        	=> 1,
			// 'autohide'    	=> 1, // Hide video controls when playing
			// 'autoplay'		=> 1, // Auto-play the video on load
			// 'loop'			=> 1, // Run the video in a loop
			// 'showinfo'		=> 0, // Hide the video title
			// 'modestbranding' => 1,  // Hide the Youtube Logo
			// );

			// $new_src = add_query_arg($params, $src);

			// $iframe = str_replace($src, $new_src, $iframe);


			// // add extra attributes to iframe html
			// $attributes = 'frameborder="0"';

			// $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

		// We want to embed this muted so we have to use the iFrame API from Youtube
			
			?>
			<script async src="https://www.youtube.com/iframe_api"></script>
			<script>
			 function onYouTubeIframeAPIReady() {
			  var player;
			  player = new YT.Player('muteYouTubeVideoPlayer', {
			    videoId: "<?php echo $src ?>", // YouTube Video ID
			    width: 640,               // Player width (in px)
			    height: 360,              // Player height (in px)
			    playerVars: {
			      autoplay: 1,        // Auto-play the video on load
			      controls: 0,        // Show pause/play buttons in player
			      showinfo: 0,        // Hide the video title
			      modestbranding: 1,  // Hide the Youtube Logo
			      loop: 1,            // Run the video in a loop
			      fs: 0,              // Hide the full screen button
			      cc_load_policy: 0, // Hide closed captions
			      iv_load_policy: 3,  // Hide the Video Annotations
			      autohide: 0,         // Hide video controls when playing
			      rel: 0,				// 0 hides related videos at the end
			    },
			    events: {
			      onReady: function(e) {
			        e.target.mute();
			      }
			    }
			  });
			 }

			 // Written by @labnol 
			 // https://www.labnol.org/internet/embed-mute-youtube-video/29149/
			
			</script>
			
		<?php
		
		echo '<div id="video-container">';
			// echo $iframe
			echo '<div id="muteYouTubeVideoPlayer"></div>';
			get_template_part('template-parts/header/content', 'display_multi_media_area_overlay'); 
		echo '</div>';

		break;
	default:
		
	$images = get_field('gallery');

	// Get the size we want 
	$size = set_image_size();

	if($images):
		echo '<div id="carousel-wrapper">';
			echo '<section id="carousel-header">';
			 	echo '<div id="carousel-feature" class="carousel slide carousel-fade" data-ride="carousel">';
				 	
				 	echo '<div class="carousel-inner" role="listbox">';
				 		if( $images ) : $i = 1;
							
				 		foreach( $images as $image ): ?>
				 		<div class="item <?php if($i == 1 ) { echo 'active'; } ?>">
						<?php	
							echo '<div class="fill" style="background-image:url(' . $image['sizes'][ $size ] . ');" alt="' . $image['alt'] . '" width="' .$image['sizes'][ $size . '-width' ]. '" height="' .$image['sizes'][ $size . '-height' ]. '" ></div>';								
							echo '<div class="sr-only slide-caption-data">' . $image['caption'] . '</div>';
						echo '</div>'; // end item
						$i++; endforeach;
						endif;
					echo '</div>'; // end .carousel-inner
				echo '</div>'; // end #carousel-feature
			echo '</section>'; // end section #carousel-header
			get_template_part('template-parts/header/content', 'display_multi_media_area_overlay'); 
		echo '</div>'// end div#carousel-wrapper
			?>
			<!-- Controls -->
			<!-- <a class="left carousel-control" href="#carousel-feature" role="button" data-slide="prev">
				<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
				<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-feature" role="button" data-slide="next">
				<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
				<span class="sr-only">Next</span>
			</a> -->
			
			<?php 
			
		endif; // ends gallery
		break;
}
?>