<?php

// START CONTENT OVERLAY FOR SUB FIELDS ON PAGE
// This section is a holy grail layout that sits over the carousel or video

// What areas are we displaying
$areas_to_display = get_field('areas_to_display');

// Get the variables.
$content_area_top_box = get_field('content_area_top_box');
$content_area_center = get_field('content_area_center');				
$content_area_aside_left = get_field('content_area_aside_left');
$content_area_aside_right = get_field('content_area_aside_right');
$content_area_bottom_box = get_field('content_area_bottom_box');



// POST RACE CONTENT BOXES

$content_area_post_race_top = get_field('content_area_post_race_top');
$content_area_post_race_bottom = get_field('content_area_post_race_bottom');
//$field = get_field_object('areas_to_display');
// $value = $areas_to_display['value'];
// $value_output = "'".implode("','", $value)."'";
// echo '<pre>';
//   print_r($areas_to_display);
// echo '</pre>';

// $holy_grail = array('top','aside_left','aside_right','center_content','bottom');
// $no_center = array('top','aside_left','aside_right','bottom');
// $left_center = array('top','aside_left','center_content','bottom');
// $no_bottom = array('top','aside_left','aside_right','center_content');

$holy_grail = 'holy_grail';
$post_race = 'post_race';
$no_center = 'no_center';
$no_bottom = 'no_bottom';

	
		// If all the options are checked, we display the 'holy grail' layout, giving the center box a flex: (3 0 0); on desktop and the asides a max-width od 24%
		if ($areas_to_display == $holy_grail) {
			echo '<div layout="column" class="overlay-content-wrapper holy_grail" id="overlay-wrapper-target">';
				echo '<div class="top-content-box" layout="rows-center" self="first">';
					echo $content_area_top_box;
				echo '</div>';
			
			echo '<div layout="lg-column" self="size-x1">';
				echo '<div class="overlay-d overlay-aside-hg left-content-box" self="size-x1">';
					echo $content_area_aside_left;
				echo '</div>';

				echo '<div class="overlay-d overlay-main-content-hg" self="size-x3 sm-first">';
					echo $content_area_center;
				echo '</div>';
				
				echo '<div class="overlay-d overlay-aside-hg right-content-box" self="size-x1">';
					echo $content_area_aside_right;
				echo '</div>';
			echo '</div>';
			
			echo '<div layout="rows-justify" class="bottom-content-box">';
				echo $content_area_bottom_box;
			echo '</div>';

			echo '</div>'; //overlay-content-wrapper
		}
		elseif ($areas_to_display == 'flex_rows') {
			if ( have_rows( 'flexbox_overlay_options_page_content_sections' ) ) : 
			echo '<div class="overlay-content-wrapper-flexbox">';
				while ( have_rows( 'flexbox_overlay_options_page_content_sections' ) ) : the_row();
						
					get_template_part('template-parts/page/flexbox', 'section');
		    		
				endwhile;
			echo '</div>';
			else :
			// no rows found
			endif; //page_content
		}
		// If center_content is not checked, we expand the asides to fill the content area. Better for mobile so there is no an empty DOM element to style around.
		elseif ($areas_to_display == $no_center) {
			echo '<div layout="column" class="overlay-content-wrapper no_center" id="overlay-wrapper-target">';
				
				echo '<div class="top-content-box" layout="rows-center" self="first" id="overlay-top">';
					echo $content_area_top_box;
				echo '</div>';
				
				echo '<div layout="sm-column md-row" id="overlay-middle">';
					
					echo '<div layout="column" class="overlay-aside left-content-box" >';
						
							echo $content_area_aside_left;
						
					echo '</div>';
					
					echo '<div layout="column" self="sm-first" class="overlay-aside right-content-box">';
						
							echo $content_area_aside_right;
						
					echo '</div>';
					
				echo '</div>';

				echo '<div id="overlay-bottom" self="bottom">';
					echo $content_area_bottom_box;
				echo '</div>';

			echo '</div>'; //overlay-content-wrapper
		}
		// If center_content is not checked, we expand the asides to fill the content area. Better for mobile so there is no an empty DOM element to style around.
		elseif ($areas_to_display == $post_race) {
			echo '<div layout="column" class="overlay-content-wrapper post_race" id="overlay-wrapper-target">';
				
				echo '<div class="top-content-box" layout="rows-center" self="first" id="overlay-top">';
					echo $content_area_top_box;
				echo '</div>';
			
				echo '<div layout="column justify-stretch" id="overlay-middle">';
					
					echo '<div class="content_area_post_race_top">';
						echo $content_area_post_race_top;
					echo '</div>';

					echo '<div class="content_area_post_race_bottom" layout="column top-left">';
						echo $content_area_post_race_bottom;
					echo '</div>';	
					
				echo '</div>';
				
				echo '<div id="overlay-bottom" self="bottom">';
					echo $content_area_bottom_box;
				echo '</div>';

			echo '</div>'; //overlay-content-wrapper

		} // Remove bottom; helpful if we do not have sponsors
		elseif ($areas_to_display == $no_bottom) {
			echo '<div layout="column" class="overlay-content-wrapper no_center" id="overlay-wrapper-target">';
				
				echo '<div class="top-content-box" layout="rows-center" self="first" id="overlay-top">';
					echo $content_area_top_box;
				echo '</div>';
				
				echo '<div layout="lg-column" self="size-x1">';
					
					echo '<div class="overlay-aside left-content-box" self="size-x1">';
						echo $content_area_aside_left;
					echo '</div>';
					echo '<div class="overlay-aside right-content-box" self="size-x1">';
						echo $content_area_aside_right;
					echo '</div>';
					
				echo '</div>';

			echo '</div>'; //overlay-content-wrapper
		}

?>